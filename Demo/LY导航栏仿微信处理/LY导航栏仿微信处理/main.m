//
//  main.m
//  LY导航栏仿微信处理
//
//  Created by aaaa on 2019/11/27.
//  Copyright © 2019 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
