//
//  AppDelegate.m
//  LY导航栏仿微信处理
//
//  Created by aaaa on 2019/11/27.
//  Copyright © 2019 aaaa. All rights reserved.
//

#import "AppDelegate.h"
#import "LLLNavigationController.h"
#import "LLLViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UINavigationController *nav = [[LLLNavigationController alloc]initWithRootViewController:[LLLViewController new]];
    nav.title = @"1";
    UINavigationController *nav1 = [[LLLNavigationController alloc]initWithRootViewController:[LLLViewController new]];
    nav1.title = @"2";
    UINavigationController *nav2 = [[LLLNavigationController alloc]initWithRootViewController:[LLLViewController new]];
    nav2.title = @"3";
    UINavigationController *nav3 = [[LLLNavigationController alloc]initWithRootViewController:[LLLViewController new]];
    nav3.title = @"4";
    UITabBarController *tabbar = [UITabBarController new];
    tabbar.viewControllers = @[nav,nav1,nav2,nav3];    
    self.window.rootViewController = tabbar;
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
