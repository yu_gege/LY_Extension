//
//  LLLViewController.m
//  LY导航栏仿微信处理
//
//  Created by aaaa on 2019/11/27.
//  Copyright © 2019 aaaa. All rights reserved.
//

#import "LLLViewController.h"

@interface LLLViewController ()

@property(nonatomic,strong)UIImageView *image;
@end

@implementation LLLViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"测试测试测试测试测试测试";
    self.view.backgroundColor = [self RandomColor];
}

- (IBAction)push:(id)sender {
    [self.navigationController pushViewController:[LLLViewController new] animated:YES];
}

- (IBAction)pop:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIColor*)RandomColor {
    
    NSInteger aRedValue =arc4random() %255;
    
    NSInteger aGreenValue =arc4random() %255;
    
    NSInteger aBlueValue =arc4random() %255;
    
    UIColor *randColor = [UIColor colorWithRed:aRedValue /255.0f green:aGreenValue /255.0f blue:aBlueValue /255.0f alpha:1.0f];
    
    return randColor;
    
}
@end
