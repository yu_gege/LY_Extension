//
//  LLLNavigationController.m
//  LY导航栏仿微信处理
//
//  Created by aaaa on 2019/11/27.
//  Copyright © 2019 aaaa. All rights reserved.
//

#import "LLLNavigationController.h"

@interface LLLNavigationController ()

@end

@implementation LLLNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //1.设置导航栏背景颜色
    [[UINavigationBar appearance] setBarTintColor:[UIColor clearColor]];
    
    //2.设置导航栏背景图片
    [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:[UIColor clearColor] size:CGSizeMake([UIScreen mainScreen].bounds.size.width, 70)] forBarMetrics:UIBarMetricsDefault];
    
    
    
    //3.设置导航栏标题样式
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                         [UIColor whiteColor],NSForegroundColorAttributeName,
                                                         [UIFont boldSystemFontOfSize:15], NSFontAttributeName, nil]];
    //4.设置导航栏返回按钮的颜色
    [[UINavigationBar appearance] setTintColor:[UIColor greenColor]];
   
    //5.分割线
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
}

- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
