//
//  DispatchQueue.swift
//  主题切换_Swift
//
//  Created by 林域 on 2021/9/18.
//  Copyright © 2021 aaaa. All rights reserved.
//

import Foundation

extension DispatchQueue {
    private static var _onceToken = [String]()
    
    class func once(token: String = "\(#file):\(#function):\(#line)", block: ()->Void) {
        
        objc_sync_enter(self)
        
        defer
        {
            objc_sync_exit(self)
        }

        if _onceToken.contains(token)
        {
            return
        }

        _onceToken.append(token)
        block()
    }
}
