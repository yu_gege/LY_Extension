//
//  UserDefaultExtension.swift
//  主题切换_Swift
//
//  Created by 林域 on 2021/9/18.
//  Copyright © 2021 aaaa. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    //Theme Config
    var currentThemeConfig: String {
        get { return (object(forKey: #function) ?? "Default" ) as! String}
        set { set(newValue, forKey: #function) }
    }
}
