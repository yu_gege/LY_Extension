//
//  ThemeManager.swift
//  主题切换_Swift
//
//  Created by 林域 on 2021/9/17.
//  Copyright © 2021 aaaa. All rights reserved.
//

import Foundation
import UIKit


class ThemeManager {
    
    static let share = ThemeManager()
        
    var themeConfigData: [String:Any]
    var hasTable = NSHashTable<UIView>.init(options: NSPointerFunctions.Options.weakMemory)
    
    init() {
        themeConfigData = AppThemeConfig.getThemeWithType(theme: AppThemeConfig.init(rawValue: UserDefaults.standard.currentThemeConfig)!)
    }
    
    func changeTheme(theme t: AppThemeConfig) {
        
        UserDefaults.standard.currentThemeConfig = t.rawValue
        themeConfigData = AppThemeConfig.getThemeWithType(theme: t)
        
        changeThemeForViewInTable()
    }
    
    private func changeThemeForViewInTable() {
        for view in hasTable.allObjects {
            
        }
    }
    
    func addViewForObserve(view v: UIView) {
        if !hasTable.contains(v) {
            hasTable.add(v)
        }
    }
    
    func removeViewForObserve(view v: UIView) {
        hasTable.remove(v)
    }
}


