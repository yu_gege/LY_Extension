//
//  ThemeConfig.swift
//  主题切换_Swift
//
//  Created by 林域 on 2021/9/18.
//  Copyright © 2021 aaaa. All rights reserved.
//

import Foundation

enum ThemeKey:String {
    case displayName
    case version
    case plainTextColor
    case commentColor
    case functionColor
    case keywordColor
    case stringColor
    case numberColor
    case variableColor
    case schemaColor
    case tableColor
    case columnColor
    case backgroundColor
    case selectionColor
    case fontName
    case fontSize
    case lineNumberBackgroundColor
    case lineNumberFontColor
}

enum AppThemeConfig: String {
   
    case Sunset
    case Dark
    case Default
    
    static func getThemeWithType(theme t: AppThemeConfig) -> [String:Any] {
        var configData: [String:Any] = [:]
        if let path = Bundle.main.path(forResource: t.rawValue, ofType: "json") {
            do{
                let data = NSData.init(contentsOfFile:path)
                let json = try JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers)
                if let d = json as? [String:Any] {
//                    print("json = \(json) = \(d["variableColor"]!)")
                    configData = d
                }
            } catch {
                print("theme json error")
            }
        }
        return configData
    }
}
