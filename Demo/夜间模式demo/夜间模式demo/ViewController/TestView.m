//
//  TestView.m
//  夜间模式demo
//
//  Created by 林域 on 2021/9/8.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import "TestView.h"
#import "BCSkinTool.h"
#import <objc/runtime.h>

@implementation TestView


-(void)removeFromSuperview{
    NSLog(@"remove from superView");
}

-(void)didMoveToSuperview{
    NSLog(@"view did move to superview");
    [[BCSkinTool shareInstance]addThemeObserveView:self];
}

@end
