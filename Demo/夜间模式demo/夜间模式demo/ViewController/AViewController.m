//
//  AViewController.m
//  夜间模式demo
//
//  Created by aaaa on 2020/4/11.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "AViewController.h"
#import <objc/runtime.h>
#import "UIView+Theme.h"
#import "UILabel+Theme.h"
#import "BCSkinTool.h"
#import "UIColor+Theme.h"
#import "TestView.h"
#import "UIImage+Theme.h"
#import "UIColor+Theme.h"
@interface AViewController ()

@end

@implementation AViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"AAA";
    
//    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 50, 50)];
//    v.backgroundColor = BCAppearenceColor(keywordColor);
//    [self.view addSubview:v];

    UILabel *view = [[UILabel alloc] initWithFrame:CGRectMake(100, 100, 100, 30)];
    view.text = @"123123";
    view.backgroundColor = BCAppearenceColor(keywordColor);
    view.textColor = BCAppearenceColor(plainTextColor);
    [self.view addSubview:view];

    NSLog(@"labek color name = %@",view.textColor.name);
    
//    Class c = [view class];
//    do {
//        [self changeThemeForView:view class:c];
//        c = class_getSuperclass(c);
//    } while ( ![c isEqual:[NSObject class]] );
    
    [self showStudentClassProperties];
    
//    UIButton *bt = [UIButton buttonWithType:UIButtonTypeSystem];
//    [bt setTitle:@"button" forState:UIControlStateNormal];
//    [bt setBackgroundImage:[UIImage imageWithThemeName:@""] forState:UIControlStateNormal];
//    [bt setTitleColor:BCAppearenceColor(commentColor) forState:UIControlStateNormal];
//    bt.frame = CGRectMake(50, 250, 50, 50);
//    [self.view addSubview:bt];
}


-(void)showStudentClassProperties
{
    Class cls = [UIView class];
    unsigned int count;
    while (cls!=[NSObject class]) {
        objc_property_t *properties = class_copyPropertyList(cls, &count);
        for (int i = 0; i<count; i++) {
            objc_property_t property = properties[i];
            NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
            NSLog(@"属性名==%@",propertyName);
        }
        if (properties){
            //要释放
           free(properties);
        }
    //得到父类的信息
    cls = class_getSuperclass(cls);
    }
}



-(void)changeThemeForView:(UIView *)view class:(Class)cls {
    
    NSLog(@"class =============== %@",NSStringFromClass(cls));
    uint count = 0;
    objc_property_t *properties = class_copyPropertyList(cls, &count);
    
    for ( int i = 0; i < count; i++ ) {
        
        NSString *name = [NSString stringWithCString:property_getName(properties[i]) encoding:NSUTF8StringEncoding];
        NSLog(@"name = %@",name);
//        if ( ![name hasPrefix:@"_"] && [name hasSuffix:@"Color"]) {
//
//        }
    }
}

- (IBAction)changeTheme:(UIButton *)sender {
//    [[BCSkinTool shareInstance] setColorThemeType:sender.tag];
    
    
}


@end

