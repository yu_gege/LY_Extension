//
//  BCThemeDefine.h
//  夜间模式demo
//
//  Created by aaaa on 2020/6/16.
//  Copyright © 2020 aaaa. All rights reserved.
//

#ifndef BCThemeDefine_h
#define BCThemeDefine_h

#import <Foundation/Foundation.h>
#import <Objc/runtime.h>

static inline void exchangeInstanceMethod(SEL A,SEL B,Class target, Class exchangeTarget) {
    Method oldMethod = class_getInstanceMethod(target, A);
    Method newMethod = class_getInstanceMethod(exchangeTarget, B);
    BOOL isAdded = class_addMethod(target, method_getName(newMethod), method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    if (isAdded) {
        class_replaceMethod(target, method_getName(oldMethod), method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    }else{
        method_exchangeImplementations(oldMethod, newMethod);
    }
}

static inline void exchangeClassMethod(SEL A,SEL B,Class target, Class exchangeTarget) {
    Method oldMethod = class_getInstanceMethod(target, A);
    Method newMethod = class_getInstanceMethod(exchangeTarget, B);
    BOOL isAdded = class_addMethod(target, method_getName(newMethod), method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    if (isAdded) {
        class_replaceMethod(target, method_getName(oldMethod), method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    }else{
        method_exchangeImplementations(oldMethod, newMethod);
    }
}


//交换方法 传入要交换的selector
#define BCThemeSwitchMethod(SEL_A,SEL_B) +(void)load{ \
     static dispatch_once_t onceToken; \
        dispatch_once(&onceToken, ^{ \
            Method originalMethod = class_getInstanceMethod([self class],SEL_A); \
            Method swizzlingMethod = class_getInstanceMethod([self class], SEL_B); \
            BOOL isAdded = class_addMethod([self class],method_getName(originalMethod),  method_getImplementation(originalMethod),method_getTypeEncoding(originalMethod)); \
            if (isAdded) {\
                class_replaceMethod([self class],method_getName(originalMethod),  method_getImplementation(swizzlingMethod),method_getTypeEncoding(swizzlingMethod)); \
            } else \
            { \
                method_exchangeImplementations(originalMethod, swizzlingMethod); \
            } \
        });\
}\

//交换多个方法 传入方法名二维数组，@[@"SEL_SYS",@"SEL_CUS"],@"SEL_OLD",@"SEL_NEW"]]
#define BCThemeSwitchMutiMethods(MethodArray) +(void)load{ \
     static dispatch_once_t onceToken; \
        dispatch_once(&onceToken, ^{ \
            for (NSArray *arr in MethodArray) { \
                Method originalMethod = class_getInstanceMethod([self class], NSSelectorFromString(arr.firstObject)); \
                Method swizzlingMethod = class_getInstanceMethod([self class], NSSelectorFromString(arr.lastObject));\
                BOOL isAdded = class_addMethod([self class],method_getName(originalMethod), method_getImplementation(originalMethod),method_getTypeEncoding(originalMethod));\
                if (isAdded)\
                {\
                    class_replaceMethod([self class],method_getName(originalMethod), method_getImplementation(swizzlingMethod),method_getTypeEncoding(swizzlingMethod));\
                } else { \
                    NSLog(@"[UIView] 方法在原类中存在，用下面的方法交换其实现方法");\
                    method_exchangeImplementations(originalMethod, swizzlingMethod);\
                }\
            } \
        });\
}\



#endif /* BCThemeDefine_h */
