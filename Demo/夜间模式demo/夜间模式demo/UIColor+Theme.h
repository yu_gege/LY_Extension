//
//  UIColor+Theme.h
//  夜间模式demo
//
//  Created by aaaa on 2020/5/28.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Theme)

#pragma mark - 为所有的color拓展一个name 用来作为key -

/// 设置16进制颜色
/// @param themeName 颜色名字
/// @param hexString 颜色
+(UIColor *)colorWithThemeName:(NSString *)themeName hexString:(NSString *)hexString;

/// 设置颜色名字
/// @param name name
-(void)setName:(NSString *)name;

/// 名字
-(NSString *)name;

@end

NS_ASSUME_NONNULL_END
