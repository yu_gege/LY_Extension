//
//  UIImage+Theme.h
//  夜间模式demo
//
//  Created by aaaa on 2020/5/28.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Theme)

/// 设置一个可以改变的图片
+ (UIImage *)imageWithThemeName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
