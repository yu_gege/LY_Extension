//
//  AppDelegate.m
//  夜间模式demo
//
//  Created by aaaa on 2020/4/11.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController/AViewController.h"
#import "ViewController/BViewController.h"
#import "ViewController/CViewController.h"
@interface AppDelegate ()

@end
@implementation AppDelegate
@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    UINavigationController *navA = [[UINavigationController alloc] initWithRootViewController:[AViewController new]];
    UINavigationController *navB = [[UINavigationController alloc] initWithRootViewController:[BViewController new]];
    UINavigationController *navC = [[UINavigationController alloc] initWithRootViewController:[CViewController new]];
    
    UITabBarController *tabbarController = [UITabBarController new];
    tabbarController.viewControllers = @[navA,navB,navC];
    self.window.rootViewController = tabbarController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
