//
//  UIImage+Theme.m
//  夜间模式demo
//
//  Created by aaaa on 2020/5/28.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "UIImage+Theme.h"
#import <Objc/runtime.h>

@implementation UIImage (Theme)

/// 设置一个可以改变的图片
+ (UIImage *)imageWithThemeName:(NSString *)name{
    UIImage *image = [UIImage imageNamed:name];
    [image setName:name];
    return image;
}

-(void)setName:(NSString *)name{
    objc_setAssociatedObject(self, @selector(name), name, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(NSString *)name{
    return objc_getAssociatedObject(self,_cmd);
}




@end
