//
//  BCSkinTool.h
//  Bcircle
//
//  Created by licong on 2019/6/13.
//  Copyright © 2019 Bcircle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ThemeType_Default,
    ThemeType_Dark,
    ThemeType_Sunset,
} ThemeType;

extern NSString *const plainTextColor;
extern NSString *const commentColor;
extern NSString *const functionColor;
extern NSString *const keywordColor;
extern NSString *const stringColor;
extern NSString *const numberColor;
extern NSString *const variableColor;
extern NSString *const schemaColor;
extern NSString *const tableColor;
extern NSString *const columnColor;
extern NSString *const backgroundColor;
extern NSString *const selectionColor;
extern NSString *const fontName;
extern NSString *const fontSize;
extern NSString *const lineNumberBackgroundColor;
extern NSString *const lineNumberFontColor;

@interface BCSkinTool : NSObject

+ (instancetype)shareInstance;

-(void)addThemeObserveView:(UIView *)view;

-(void)removeThemeObserveView:(UIView *)view;


-(void)setColorThemeType:(ThemeType)themeType;

-(UIColor *)getColorWithName:(NSString *)name;

-(UIImage *)getImageWithName:(NSString *)imageName;

@end

/// 设置可变的字体颜色
/// @param colorName  颜色名字
static inline UIColor * BCAppearenceColor(NSString *colorName){
    return [[BCSkinTool shareInstance] getColorWithName:colorName];
}

/// 设置可变的图片
/// @param imageName 图片名字
static inline UIImage * BCAppearenceImage(NSString *imageName){
    return [[BCSkinTool shareInstance] getImageWithName:imageName];
}


NS_ASSUME_NONNULL_END
