//
//  UIView+Theme.h
//  夜间模式demo
//
//  Created by aaaa on 2020/5/28.
//  Copyright © 2020 aaaa. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface UIView (Theme)

@property(nonatomic,strong)UIColor *backgroundColor;

-(UIColor *)getBackgroundColor;

@end

NS_ASSUME_NONNULL_END
