//
//  SymtemColorViewController.m
//  夜间模式（系统）
//
//  Created by aaaa on 2020/5/27.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "SymtemColorViewController.h"
#import "SystemColorCollectionViewCell.h"

@interface SymtemColorViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation SymtemColorViewController
{
    NSArray<NSArray<UIColor *> *> *colors;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    //展示系统的颜色
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.itemSize = CGSizeMake(30,30);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    
    UICollectionView *collectionview = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 300) collectionViewLayout:layout];
    collectionview.delegate = self;
    collectionview.dataSource = self;
    collectionview.backgroundColor = self.view.backgroundColor;
    [collectionview registerNib:[UINib nibWithNibName:@"SystemColorCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SystemColorCollectionViewCell"];
    [self.view addSubview:collectionview];
    colors = @[[self systemColors],[self textColors]];
    [collectionview reloadData];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return colors.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return colors[section].count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SystemColorCollectionViewCell *c = [collectionView dequeueReusableCellWithReuseIdentifier:@"SystemColorCollectionViewCell" forIndexPath:indexPath];
    if (indexPath.section) {
        c.backgroundColor = [UIColor purpleColor];
        c.label.textColor = colors[indexPath.section][indexPath.row];
        c.label.text = @"A";
    }else{
        c.label.text = @"";
        c.backgroundColor = colors[indexPath.section][indexPath.row];
    }
    return c;
}

-(NSArray<UIColor *> *)systemColors{
    return @[
    UIColor.systemRedColor,
    UIColor.systemGreenColor,
    UIColor.systemBlueColor,
    UIColor.systemOrangeColor,
    UIColor.systemYellowColor,
    UIColor.systemPinkColor,
    UIColor.systemPurpleColor,
    UIColor.systemTealColor,
    UIColor.systemIndigoColor,
    UIColor.systemGrayColor,
    UIColor.systemGray2Color,
    UIColor.systemGray3Color,
    UIColor.systemGray4Color,
    UIColor.systemGray5Color,
    UIColor.systemGray6Color,
    UIColor.labelColor,
    UIColor.secondaryLabelColor,
    UIColor.tertiaryLabelColor,
    UIColor.quaternaryLabelColor,
    UIColor.linkColor,
    UIColor.placeholderTextColor,
    UIColor.separatorColor,
    UIColor.opaqueSeparatorColor,
    UIColor.systemBackgroundColor,
    UIColor.secondarySystemBackgroundColor,
    UIColor.tertiarySystemBackgroundColor,
    UIColor.systemGroupedBackgroundColor,
    UIColor.secondarySystemGroupedBackgroundColor,
    UIColor.tertiarySystemGroupedBackgroundColor,
    UIColor.secondarySystemFillColor,
    UIColor.tertiarySystemFillColor,
    UIColor.quaternarySystemFillColor,
    ];
}

-(NSArray<UIColor *> *)textColors{
    return @[UIColor.labelColor,UIColor.lightTextColor,UIColor.darkGrayColor];
}
@end
