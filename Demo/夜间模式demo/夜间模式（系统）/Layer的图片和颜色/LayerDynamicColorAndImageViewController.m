//
//  LayerDynamicColorAndImageViewController.m
//  夜间模式（系统）
//
//  Created by aaaa on 2020/5/28.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "LayerDynamicColorAndImageViewController.h"

@interface LayerDynamicColorAndImageViewController ()

@end

@implementation LayerDynamicColorAndImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    CALayer *layer1 = [CALayer new];
//    layer1.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
//        if (traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
//            return [UIColor grayColor];
//        }
//        return [UIColor blackColor];
//    }];
    layer1.frame = CGRectMake(100, 100, 100 , 100);
    [self.view.layer addSublayer:layer1];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
