//
//  DynamicColorViewController.m
//  夜间模式（系统）
//
//  Created by aaaa on 2020/5/27.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "DynamicColorViewController.h"

@interface DynamicColorViewController ()

@end

@implementation DynamicColorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ///方式一、设置colorset
    UIView *v1 = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    v1.backgroundColor = [UIColor colorNamed:@"TitleColor"];
    [self.view addSubview:v1];
    
    ///方式二、动态添加颜色
    UIView *v2 = [[UIView alloc] initWithFrame:CGRectMake(100, 250, 100, 100)];
    v2.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
        if (traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark) {
            return [UIColor redColor];
        }else{
            return [UIColor greenColor];
        }
    }];
    [self.view addSubview:v2];
}



@end
