//
//  ViewController.m
//  夜间模式（系统）
//
//  Created by aaaa on 2020/5/27.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "ViewController.h"
#define ScreenW [UIScreen mainScreen].bounds.size.width
#define ScreenH [UIScreen mainScreen].bounds.size.height

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController
{
    NSArray *arr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    arr = @[@"SymtemColorViewController",
            @"DynamicColorViewController",@"DynamicImageViewController",@"LayerDynamicColorAndImageViewController"];
    [_tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self presentViewController:[NSClassFromString([arr objectAtIndex:indexPath.row]) new] animated:YES completion:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *c = [tableView dequeueReusableCellWithIdentifier:@"ly"];
    if (!c) {
        c = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"ly"];
    }
    c.textLabel.text = [arr objectAtIndex:indexPath.row];
    return c;
}


@end
