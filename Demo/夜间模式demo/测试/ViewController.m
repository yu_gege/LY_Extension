//
//  ViewController.m
//  测试
//
//  Created by 林域 on 2021/9/10.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Theme.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 50, 50)];
    [self.view addSubview:v];
    [v removeFromSuperview];
    
}


@end
