//
//  DownloadTestCell.h
//  断点上传和下载
//
//  Created by aaaa on 2020/5/14.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DownloadTestCell : UITableViewCell

@property(nonatomic,copy)NSString *url;

@end

NS_ASSUME_NONNULL_END
