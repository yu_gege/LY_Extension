//
//  DownloadTestViewController.m
//  断点上传和下载
//
//  Created by aaaa on 2020/5/14.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "DownloadTestViewController.h"
#import "DownloadTestCell.h"

@interface DownloadTestViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray<NSString *> *urls;
@end
@implementation DownloadTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_tableView registerNib:[UINib nibWithNibName:@"DownloadTestCell" bundle:nil] forCellReuseIdentifier:@"DownloadTestCell"];
    _urls = @[
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CmqAPxkuAAv0PjQy6ao93.jpeg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CmaACg2ZAAoG9Cj1mW045.jpeg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CmOABEadAAx3WWt-DqM000.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CmCAKfL3AAgtHG-1Ypc758.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169Cl2AH5q5ABPl-A0y53s746.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/0F/wKhQo169ClqAS0JiAAUxul-w3Pg552.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/0F/wKhQo169CleAQ8QgAAkwkQ716sQ338.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/0F/wKhQo169ClCAV4yrABg2znv-uKQ062.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/0F/wKhQo169CkWADnEMAAVJ1G_r3rU567.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169Cm2Ad4sMAAsOizDUc60342.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CnGARTsvAAS6zQgb2gQ739.jpg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CnSAPDdEAAj3DFhKe_Y47.jpeg",
    @"https://test.bcirclepay.com:8443/group1/M00/00/10/wKhQo169CneAI8xHABUr4FTBdeM271.png"].mutableCopy;
    [_tableView reloadData];
}

- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _urls.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DownloadTestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DownloadTestCell"];
    cell.url = [_urls objectAtIndex:indexPath.row];
    return cell;
}



@end
