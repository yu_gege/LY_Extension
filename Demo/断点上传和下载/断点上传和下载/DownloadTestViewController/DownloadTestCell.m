//
//  DownloadTestCell.m
//  断点上传和下载
//
//  Created by aaaa on 2020/5/14.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "DownloadTestCell.h"
#import "BCResourceDownloader.h"
#import "BCResourceItem.h"

@interface DownloadTestCell ()
@property (weak, nonatomic) IBOutlet UIButton *stateButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressMargin;
@property (weak, nonatomic) IBOutlet UILabel *reciveLengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLengthLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *urlLabel;
@property(nonatomic,weak)BCResourceItem *downloadItem;
@end

@implementation DownloadTestCell
- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)dealloc{
    NSLog(@"cell dealloc");
    [self removeItem];
}

-(void)setUrl:(NSString *)url{
    _url = url;
    _urlLabel.text = _url.lastPathComponent;
    [self removeItem];
            
    _downloadItem = [[BCResourceDownloader shareDownloader] downloadResourceWithUrl:_url allowContinue:YES progress:nil result:nil];
    _reciveLengthLabel.text = [NSString stringWithFormat:@"%lu",_downloadItem.recieveLength];
    _totalLengthLabel.text = [NSString stringWithFormat:@"%lu",_downloadItem.totalLength];
    _progressMargin.constant = 200 - _downloadItem.progress * 200;
    _imgView.image = [UIImage imageWithContentsOfFile:[_downloadItem savePath]];
    
    __weak typeof(self) weakself = self;
    _downloadItem.progressCallBack = ^(NSInteger receivedSize, NSInteger expectedSize, CGFloat progress) {
        NSLog(@"fileName = %@ receivedSize = %lu expectedSize = %lu,progress = %f",weakself.url.lastPathComponent,receivedSize,expectedSize,progress);
        weakself.reciveLengthLabel.text = [NSString stringWithFormat:@"%lu",receivedSize];
        weakself.totalLengthLabel.text = [NSString stringWithFormat:@"%lu",expectedSize];
        weakself.progressMargin.constant = 200 - progress * 200;
    };

    _downloadItem.resultCallBack = ^(BOOL isSuccess, NSError * _Nullable error, NSString * _Nullable errorDescription) {
//        NSLog(@"下载结束 : fileName = %@ 结果 = %@,原因 = %@",weakself.url.lastPathComponent,isSuccess ? @"成功" : @"失败",errorDescription);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            UIImage *image = [UIImage imageWithContentsOfFile:[weakself.downloadItem savePath]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                weakself.imgView.image = image;
                [weakself setState];
            });
        });
    };
    
    [self setState];
}

-(void)setState{
    NSString *str = @"";
    switch (_downloadItem.downloadState){
        case BCDownloadState_unDownload:
            str = @"未下载";
            break;
        case BCDownloadState_Failed:
            str = @"下载失败";
            break;
        case BCDownloadState_Wating:
            str = @"等待中";
            break;
        case BCDownloadState_NotAllow:
            str = @"网络不支持";
            break;
        case BCDownloadState_Success:
            str = @"下载完成";
            break;
        case BCDownloadState_Suspending:
            str = @"暂停中";
            break;
        case BCDownloadState_Downloading:
            str = @"下载中";
            break;
    }
    [_stateButton setTitle:str forState:UIControlStateNormal];
}

- (IBAction)click:(id)sender {
    if (_downloadItem) {
        switch (_downloadItem.downloadState){
            case BCDownloadState_unDownload:
                [[BCResourceDownloader shareDownloader] startDownloadWithUrl:_url];
                break;
            case BCDownloadState_Failed:
                [[BCResourceDownloader shareDownloader] startDownloadWithUrl:_url];
                break;
            case BCDownloadState_Wating:
                
                break;
            case BCDownloadState_NotAllow:
                
                break;
            case BCDownloadState_Success:
                
                break;
            case BCDownloadState_Suspending:
                [[BCResourceDownloader shareDownloader] continuteDownloadWithUrl:_url];
                break;
            case BCDownloadState_Downloading:
                [[BCResourceDownloader shareDownloader] pauseDownloadWithUrl:_url];                
                break;
        }
    }
    [self setState];
}

-(void)removeItem{
    _downloadItem.progressCallBack = nil;
    _downloadItem.resultCallBack = nil;
    _downloadItem = nil;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
