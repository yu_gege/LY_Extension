//
//  BCDownloadHelper.h
//  断点上传和下载
//
//  Created by aaaa on 2020/3/19.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCDownloadHelper : NSObject

+ (instancetype)defaultHelper;

@property(nonatomic,strong,readonly)NSDictionary *taskMap;

//断点下载
+ (NSURLSessionDownloadTask *)downloadWithUrl:(NSURL *)url
                                     progress:(void(^)(NSUInteger currentLength,NSUInteger totalLength))progress
                                       result:(void(^)(BOOL isSuccess,NSError *error))result;
//取消下载
+ (void)cancleTaskWithUrl:(NSURL *)url;

//暂停下载
+ (void)pauseTaskWithUrl:(NSURL *)url;

//启动下载
+ (void)resumeTaskWithUrl:(NSURL *)url;

//文件大小
+ (NSInteger)getCacheFileLengthWithUrl:(NSString *)url;
@end

NS_ASSUME_NONNULL_END
