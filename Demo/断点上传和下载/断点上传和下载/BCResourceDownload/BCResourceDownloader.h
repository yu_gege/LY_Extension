//
//  BCResourceDownloader.h
//  断点上传和下载
//
//  Created by aaaa on 2020/5/13.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class BCResourceItem;

//下载状态
typedef enum : NSInteger {
    BCDownloadState_Failed = -1,
    BCDownloadState_unDownload, // 未下载
    BCDownloadState_Success, // 下载成功
    BCDownloadState_Wating, //等待下载
    BCDownloadState_Downloading,//下载中
    BCDownloadState_Suspending,//暂停中
    BCDownloadState_NotAllow,//不允许(网络环境问题、操作环境问题比如正在通话)
} BCDownloadState;

//上传状态
typedef enum : NSInteger {
    BCUploadState_Failed = -1, // 
    BCUploadState_unUpload, // 未开始上传
    BCUploadState_Success, // 上传成功
    BCUploadState_Wating, // 等待上传
    BCUploadState_Uploading, // 上传中
    BCUploadState_Suspending, // 挂起中
    BCUploadState_NotAllow,//不允许(网络环境问题、操作环境问题比如正在通话)
} BCUploadState;

//下载
typedef void(^ __nullable BCResouceProgress)(long receivedSize, long expectedSize, CGFloat progress);
typedef void(^ __nullable BCResouceResult)(BOOL isSuccess,NSError *__nullable error,NSString *__nullable errorDescription);

NS_ASSUME_NONNULL_BEGIN

@interface BCResourceDownloader : NSObject


+(instancetype)shareDownloader;

@property(nonatomic,strong)NSURLSession *session;

#pragma mark - config -
/// 设置最大下载数量
/// @param maxDownloadNum 最大不超过16
-(void)setMaxDownloadNum:(uint16_t)maxDownloadNum;


/// 设置允许蜂窝下载
/// @param allowInWLAN default is YES
-(void)setAllowDownloadInWLAN:(BOOL)allowInWLAN;


/// 设置允许各种环境都能下载的大小
/// @param byteLength default is MaxFloat
-(void)setAllowDownloadTaskSize:(long)byteLength;


#pragma mark - download -
/// 下载
/// @param url url
/// @param allowContinue 是否支持断点下载
/// @param progress 进度回调
/// @param result 下载结果
-(BCResourceItem *)downloadResourceWithUrl:(NSString *)url
                 allowContinue:(BOOL)allowContinue
                      progress:(BCResouceProgress __nullable)progress
                        result:(BCResouceResult __nullable)result;

/* ----- ⚠️代码中的《暂停》跟《取消》是一样的 因为suspend并不是真正的暂停  ------ */
/* ----- 这里的暂停和挂起 有轻微的区别 挂起代表’被打断性‘的暂停，比如正在执行一个需要良好网络环境的操作 需要暂时性的把正在下载的任务中断，操作完成后要继续 ------ */
// -----------------------------------

@property (nonatomic, copy) void (^completionHandler)(void);



//开始某个下载
-(void)startDownloadWithUrl:(NSString *)url;

//暂停某个下载
-(void)pauseDownloadWithUrl:(NSString *)url;

//暂停所有下载
-(void)pauseAllDownloadTask;

//继续某个下载
-(void)continuteDownloadWithUrl:(NSString *)url;

//继续全部下载
-(void)continuteAllDownloadTask;

// -----------------------------------

//挂起所有下载中的任务
-(void)suspendAllDownlondingTask;

//继续被挂起的任务
-(void)continuteAllSuspendTask;

// -----------------------------------

//取消某个下载
-(void)cancleDownloadWithUrl:(NSString *)url;

//取消所有下载
-(void)cancelAllDownloadTask;

//存在某个下载任务
-(BOOL)existDownloadTaskWithUrl:(NSString *)url;;

#pragma mark - upload -
/// 上传
/// @param url url
/// @param filePath 文件路径
/// @param allowBackground 是否支持后台
/// @param progress 进度回调
/// @param result 结果回调
-(void)uploadResourceWithUrl:(NSString *)url
                    filePath:(NSString *)filePath
             allowBackground:(BOOL)allowBackground
                    progress:(BCResouceProgress)progress
                      result:(BCResouceResult)result;



/// 分片上传
/// @param url url
/// @param filePath 文件路径
/// @param allowBackground 是否支持后台
/// @param progress 进度回调
/// @param result 结果回调
-(void)uploadResourceBySeperateDataWithUrl:(NSString *)url
                                  filePath:(NSString *)filePath
                           allowBackground:(BOOL)allowBackground
                                  progress:(BCResouceProgress)progress
                                    result:(BCResouceResult)result;
@end

NS_ASSUME_NONNULL_END
