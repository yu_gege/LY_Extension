//
//  BCResourceDownloadHelper.h
//  断点上传和下载
//
//  Created by aaaa on 2020/5/7.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCResourceDownloadHelper : NSObject

/// 只在下载的时候转换一下 其他的还是得用原来的URL作为key 以免多个地方用到url需要不同的转换
+(NSString *)handleUrl:(NSString *)url;


+(NSString *)getDownloadInfoCachePathWithUrl:(NSString *)url;


+(NSString *)getSavePathWithUrl:(NSString *)url;



@end

NS_ASSUME_NONNULL_END
