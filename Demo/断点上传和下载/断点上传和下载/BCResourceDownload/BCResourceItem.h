//
//  BCResourceItem.h
//  断点上传和下载
//
//  Created by aaaa on 2020/5/8.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCResourceDownloader.h"
@class BCResourceDownloader;

NS_ASSUME_NONNULL_BEGIN

@interface BCResourceItem : NSObject

- (instancetype)initWithUrl:(NSString *)url;

/// url
@property(nonatomic,copy)NSString *url;

/// 下载状态
@property(nonatomic,assign)BCDownloadState downloadState;

/// 上传进度
@property(nonatomic,assign)BCUploadState uploadState;

/// 缓存路径
@property(nonatomic,copy)NSString *cachePath;

/// 保存路径
@property(nonatomic,copy)NSString *savePath;

/// 总长度
@property(nonatomic,assign)long totalLength;

/// 接收的长度
@property(nonatomic,assign)long recieveLength;

/// 进度
@property(nonatomic,assign)CGFloat progress;

/// 下载进度回调
@property(nonatomic,copy)BCResouceProgress progressCallBack;

/// 下载状态回调
@property (nonatomic, copy)BCResouceResult resultCallBack;

/// 下载
@property(nonatomic,strong)NSURLSessionDownloadTask *downloadTask;

/// 上传
@property(nonatomic,strong)NSURLSessionUploadTask *uploadTask;

/// resumedata
@property(nonatomic,strong)__block NSData *resumeData;

/// 获取之前下载的数据
+(NSArray<BCResourceItem *> *)getUnfinishedResourceItemInCacheWithTask:(NSArray<NSURLSessionDownloadTask *>  *)tasks;

/// 保存下载信息
-(void)saveDownloadInfo;

/// 是否存在本地文件
-(BOOL)existLocalFile;

/// 暂停下载
-(void)suspendTask;

/// 取消下载
-(void)cancelTask;

/// 开始下载
-(void)resumeTask;

/// 结束下载
-(void)finishTask;

/// 下载失败没有resumedata
-(void)failTask;
@end

NS_ASSUME_NONNULL_END
