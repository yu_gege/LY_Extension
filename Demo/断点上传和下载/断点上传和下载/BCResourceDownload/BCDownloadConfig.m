//
//  BCDownloadConfig.m
//  断点上传和下载
//
//  Created by aaaa on 2020/5/6.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCDownloadConfig.h"

NSString *const BCMaxDownloadCount = @"BCMaxDownloadCount";
NSString *const BCAllowDownloadInWLAN = @"BCAllowDownloadInWLAN";
NSString *const BCAllowDownloadTaskMaxSize = @"BCAllowDownloadTaskMaxSize";

uint16_t const concurrentDownloadCount = 16;
long const downloadLimitSize =  1000 * 1000 * 1000;

@interface BCDownloadConfig ()
//最大并发下载量
@property(nonatomic,assign)uint16_t maxDownloadCount;
//是否在蜂窝下下载
@property(nonatomic,assign)BOOL allowInWLAN;
//能在所有可在所有网络随便下载的最大字节长度
@property(nonatomic,assign)long allowDownloadTaskMaxSize;

@end
@implementation BCDownloadConfig
#warning check 配置是跟随系统还是用户

#pragma mark - setter -
/// 设置最大下载数量
/// @param maxDownloadCount  最大不超过16
+(void)setMaxDownloadCount:(uint16_t)maxDownloadCount{
    [[NSUserDefaults standardUserDefaults]setObject:@(maxDownloadCount) forKey:BCMaxDownloadCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/// 设置允许蜂窝下载
/// @param allowInWLAN default is YES
+(void)setAllowInWLAN:(BOOL)allowInWLAN{
    [[NSUserDefaults standardUserDefaults]setBool:allowInWLAN forKey:BCAllowDownloadInWLAN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/// 设置允许各种环境都能下载的大小
/// @param allowDownloadTaskMaxSize default is MaxFloat
+(void)setAllowDownloadTaskMaxSize:(long)allowDownloadTaskMaxSize{
    [[NSUserDefaults standardUserDefaults]setObject:@(allowDownloadTaskMaxSize) forKey:BCAllowDownloadTaskMaxSize];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#warning check 配置是跟随系统还是用户
#pragma mark - getter -
+(uint16_t)maxDownloadCount{
    uint16_t count = [[[NSUserDefaults standardUserDefaults] objectForKey:BCMaxDownloadCount] intValue];
    return count == 0 ? concurrentDownloadCount : MIN(count, concurrentDownloadCount);
}

+(BOOL)allowInWLAN{
    return [[NSUserDefaults standardUserDefaults] boolForKey:BCAllowDownloadInWLAN];
}

+(long)allowDownloadTaskMaxSize{
    long size = [[[NSUserDefaults standardUserDefaults] objectForKey:BCAllowDownloadTaskMaxSize] longValue];
    return size == 0 ? downloadLimitSize : size;
}
@end
