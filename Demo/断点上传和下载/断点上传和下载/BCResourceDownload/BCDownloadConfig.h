//
//  BCDownloadConfig.h
//  断点上传和下载
//
//  Created by aaaa on 2020/5/6.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCDownloadConfig : NSObject

#pragma mark - setter -
/// 设置最大下载数量
/// @param maxDownloadCount  最大不超过16
+(void)setMaxDownloadCount:(uint16_t)maxDownloadCount;

/// 设置允许蜂窝下载
/// @param allowInWLAN default is YES
+(void)setAllowInWLAN:(BOOL)allowInWLAN;

/// 设置允许各种环境都能下载的大小
/// @param allowDownloadTaskMaxSize default is MaxFloat
+(void)setAllowDownloadTaskMaxSize:(long)allowDownloadTaskMaxSize;


#pragma mark - getter -
//最大并发下载量
+(uint16_t)maxDownloadCount;

//是否在蜂窝下下载
+(BOOL)allowInWLAN;

//能在所有可在所有网络随便下载的最大字节长度
+(long)allowDownloadTaskMaxSize;

@end

NS_ASSUME_NONNULL_END
