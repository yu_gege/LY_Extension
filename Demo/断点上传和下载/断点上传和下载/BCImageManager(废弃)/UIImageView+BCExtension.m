//
//  UIImageView+BCExtension.m
//  断点上传和下载
//
//  Created by aaaa on 2020/4/22.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "UIImageView+BCExtension.h"

@implementation UIImageView (BCExtension)

-(void)bc_setImageWithUrl:(NSString *)url
         downloadProgress:(ImageDownloadProgress)downloadProgress
                   result:(ImageDownloadResult)result{
    [self bc_setImageWithUrl:url downloadProgress:downloadProgress transform:nil result:result];
}


-(void)bc_setImageWithUrl:(NSString *)url
         downloadProgress:(ImageDownloadProgress)downloadProgress
                transform:(ImageTransform)transform
                   result:(ImageDownloadResult)result{
    
}

@end
