//
//  Downloader.h
//  断点上传和下载
//
//  Created by aaaa on 2020/4/21.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    DownloadStateFailed = -1,   /** 下载失败 */
    DownloadStateStart,         /** 下载中 */
    DownloadStateSuspended,     /** 下载暂停 */
    DownloadStateCompleted,     /** 下载完成 */
    DownloadStateWaiting,       /** 等待下载 */
}DownloadState;

@interface BCSessionModel : NSObject

/** 流 */
@property (nonatomic, strong) NSOutputStream *stream;

/** 下载地址 */
@property (nonatomic, copy) NSString *url;

/** 获得服务器这次请求 返回数据的总长度 */
@property (nonatomic, assign) NSInteger totalLength;

/** 下载进度 */
@property (nonatomic, copy) void(^progressBlock)(NSInteger receivedSize, NSInteger expectedSize, CGFloat progress);

/** 下载状态 */
@property (nonatomic, copy) void(^stateBlock)(DownloadState state);
@end

@interface Downloader : NSObject

+ (instancetype)sharedInstance;

- (void)download:(NSString *)url
        progress:(void (^)(NSInteger recieveLength, NSInteger totalLength, CGFloat progress))progressBlock
           state:(void (^)(DownloadState))stateBlock;

@end

NS_ASSUME_NONNULL_END

