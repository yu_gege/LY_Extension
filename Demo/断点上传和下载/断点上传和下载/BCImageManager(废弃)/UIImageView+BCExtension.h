//
//  UIImageView+BCExtension.h
//  断点上传和下载
//
//  Created by aaaa on 2020/4/22.
//  Copyright © 2020 aaaa. All rights reserved.
//


#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ImageDownloadProgress)(long recievedSize,long expectedSize,CGFloat progress);

typedef void(^ImageDownloadResult)(UIImage *image, NSString * url,NSString *errorDescription);

typedef UIImage *_Nullable(^ImageTransform)(UIImage *image, NSString * url);

@interface UIImageView (BCExtension)

-(void)bc_setImageWithUrl:(nullable NSString *)url
         downloadProgress:(nullable ImageDownloadProgress)downloadProgress
                   result:(nullable ImageDownloadResult)result;

-(void)bc_setImageWithUrl:(nullable NSString *)url
         downloadProgress:(nullable ImageDownloadProgress)downloadProgress
                transform:(nullable ImageTransform)transform
                   result:(nullable ImageDownloadResult)result;
@end

NS_ASSUME_NONNULL_END
