//
//  ViewController.m
//  断点上传和下载
//
//  Created by aaaa on 2020/3/18.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "ViewController.h"
#import "BCDownloadHelper.h"
#import "BCProgressCircleView.h"
#import "BCDownloadHelper.h"
#import "BCResourceItem.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property(nonatomic,strong)NSURLSessionDownloadTask *task;
@property(nonatomic,strong)BCProgressCircleView *circleView;
@end

@implementation ViewController
{
    BCResourceItem *_item;
}
//static NSString *url = @"http://dldir1.qq.com/qqfile/QQforMac/QQ_V5.4.0.dmg";

static NSString *url = @"https://test.bcirclepay.com:8443/group1/M00/00/61/OzhuWl5XjfSEUnCfAAAAAC0C74046.jpeg?attname=1582796274585736.jpeg_80x80.jpeg";

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 11,*)) {
        
    }
    _circleView = [[BCProgressCircleView alloc] initWithFrame:CGRectMake(50, 50, 100, 100)];
    [self.view addSubview:_circleView];
    [self download];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self presentViewController:[NSClassFromString(@"DownloadTestViewController") new] animated:YES completion:nil];
}

-(void)download{
    __weak typeof(self) weakself = self;
    _item = [[BCResourceDownloader shareDownloader] downloadResourceWithUrl:url allowContinue:YES progress:^(NSInteger receivedSize, NSInteger expectedSize, CGFloat progress) {
        NSLog(@"%ld - %ld - %f",receivedSize,expectedSize,progress);
        [weakself.circleView setProgress:progress];
    } result:^(BOOL isSuccess, NSError * _Nullable error, NSString * _Nullable errorDescription) {
        NSLog(@"result");
    }];
    
    NSLog(@"%@",_item);
}

- (IBAction)download:(UIButton *)sender {
    if ([[BCResourceDownloader shareDownloader] existDownloadTaskWithUrl:url]) {
        [[BCResourceDownloader shareDownloader] startDownloadWithUrl:url];
    }else{
        [self download];
    }
}

- (IBAction)suspend:(id)sender {
    [[BCResourceDownloader shareDownloader] pauseDownloadWithUrl:url];
}

- (IBAction)cancle:(id)sender {
    [[BCResourceDownloader shareDownloader] cancleDownloadWithUrl:url];
}

@end
