//
//  main.m
//  消息转发
//
//  Created by 林域 on 2021/8/30.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnrecognizeSelector/UnrecognizeSelectorHandle.h"

void demo01(void);
void signatureDemo(void);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        demo01();
//        signatureDemo();
    }
    return 0;
}

// 转发一个不存在的实例方法
void demo01(void) {
    UnrecognizeSelectorHandle *hanlde = [UnrecognizeSelectorHandle new];
    [hanlde performSelector:NSSelectorFromString(@"unexistingInstanceMethod:")];
}

// NSMethodSignature，用来封装方法的参数、返回类型，主要用于NSInvokation
void signatureDemo(void) {
    UnrecognizeSelectorHandle *hanlde = [UnrecognizeSelectorHandle new];
    NSMethodSignature * s = [hanlde methodSignatureForSelector:@selector(existingInstanceMethod:)];
    NSLog(@"numberOfArguments = %lu",(unsigned long)s.numberOfArguments);//方法参数个数
    NSLog(@"getArgumentTypeAtIndex = %s",[s getArgumentTypeAtIndex:0]);//方法参数类型
    NSLog(@"methodReturnType = %s",s.methodReturnType);//返回值类型
    NSLog(@"methodReturnLength = %lu",(unsigned long)s.methodReturnLength);//返回值长度
}
