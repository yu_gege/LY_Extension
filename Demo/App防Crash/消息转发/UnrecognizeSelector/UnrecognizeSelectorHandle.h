//
//  UnrecognizeSelectorHandle.h
//  App防Crash
//
//  Created by 林域 on 2021/8/30.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UnrecognizeSelectorHandle : NSObject

-(void)existingInstanceMethod:(int)num;

@end

NS_ASSUME_NONNULL_END
