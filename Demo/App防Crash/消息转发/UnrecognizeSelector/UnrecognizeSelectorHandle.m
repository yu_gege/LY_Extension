//
//  UnrecognizeSelectorHandle.m
//  App防Crash
//
//  Created by 林域 on 2021/8/30.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import "UnrecognizeSelectorHandle.h"

@implementation UnrecognizeSelectorHandle

-(void)existingInstanceMethod:(int)num{
    printf("existingInstanceMethod \n");
}

#pragma mark - 1、首先进行动态方法决议 -
//对象方法
+(BOOL)resolveInstanceMethod:(SEL)sel {
    return YES;
}

//累方法
+(BOOL)resolveClassMethod:(SEL)sel{
    return YES;
}


-(id)forwardingTargetForSelector:(SEL)aSelector{
    if ([self respondsToSelector:aSelector]) {
        return self;
    }else{
        return nil;
    }
}

-(NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    NSMethodSignature *signature = [super methodSignatureForSelector:aSelector];
    if (!signature) {
        //返回一个新的方法签名
        signature = [NSMethodSignature signatureWithObjCTypes:"v@:"];
    }
    return signature;
}

-(void)forwardInvocation:(NSInvocation *)anInvocation{
    anInvocation.target = self; //指定执行的对象
    anInvocation.selector = @selector(catchUnrecognizeMethod);
    [anInvocation invoke];
}

-(void)catchUnrecognizeMethod {
    NSLog(@"%@",NSStringFromSelector(_cmd));
}


@end


