//
//  TestObj.h
//  方法签名
//
//  Created by 林域 on 2021/9/1.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestObj : NSObject

#pragma mark - demo01 -
-(void)instanceMethod01:(int)num;
-(void)instanceMethod02:(int)num;
-(void)classMethod:(int)num;

#pragma mark - demo02 -
-(void)testMethod;
-(void)testMethod:(int)num;
-(void)testMethod:(int)num digit:(double)digit;
-(void)existMethod:(NSString *)str num:(int)num;

@end

NS_ASSUME_NONNULL_END
