//
//  TestObj.m
//  方法签名
//
//  Created by 林域 on 2021/9/1.
//  Copyright © 2021 aaaa. All rights reserved.
//

#import "TestObj.h"

@implementation TestObj

-(void)instanceMethod01:(int)num {
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)instanceMethod02:(int)num {
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

-(void)classMethod:(int)num {
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

#pragma mark - demo02 -
-(void)testMethod{
    NSLog(@"test method");
}

-(void)testMethod:(int)num{
    NSLog(@"test method with int argument");
}

-(void)testMethod:(int)num digit:(double)digit{
    NSLog(@"test method with int = %d & double argument = %lf",num,digit);
}


-(void)existMethod:(NSString *)str num:(int)num {
    NSLog(@"str = %@ num = %d",str,num);
}




@end
