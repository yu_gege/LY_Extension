//
//  BCClipImageViewController.m
//  图片编辑
//
//  Created by aaaa on 2020/3/25.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCClipImageViewController.h"
#import "BCClipGridView.h"
#import "BCImageEditorHelper.h"
#import "BCZoomView.h"

#define ClipMaxZoon_horizontalMargin 20
#define ClipMaxZoon_TopMargin 20
#define ClipMaxZoon_BottomMargin 10

@interface BCClipImageViewController ()<BCGridScaleProtocol>

@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@property(nonatomic,assign) CGRect maxSize;

@property(nonatomic,strong) BCClipGridView *gridView;

@property(nonatomic,strong) BCZoomView *zoomView;

@property(nonatomic,strong) UIImageView *imageView;

@property(nonatomic,strong) UIImage *image;

@end
@implementation BCClipImageViewController
-(instancetype)initWithImage:(UIImage *)image{
    if (self = [super init]) {
        _image = image;
    }
    return self;
}

- (void)viewDidLoad {
   [super viewDidLoad];
   self.automaticallyAdjustsScrollViewInsets = NO;
   [self setupUI];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
     _maxSize = CGRectMake(ClipMaxZoon_horizontalMargin, ClipMaxZoon_TopMargin, ScreenW - ClipMaxZoon_horizontalMargin * 2, _stackView.top - ClipMaxZoon_BottomMargin - ClipMaxZoon_TopMargin);
    
    //居中一下
    CGRect zoomRect = (CGRect){CGPointMake(ClipMaxZoon_horizontalMargin, _maxSize.size.height * .5 - _imageView.height * .5),_zoomView.size};
    _zoomView.frame = zoomRect;
    
    CGRect rect  = [_zoomView convertRect:_imageView.frame toViewOrWindow:_zoomView];
    CGRect rect1 = [_zoomView convertRect:rect toViewOrWindow:self.view];
    [_gridView updateRect:rect1];
    NSLog(@"%@",NSStringFromCGRect(rect1));
    [_gridView updateLimitRect:_maxSize];
}

#pragma mark - setup ui -
-(void)setupUI{
    
    _maxSize = CGRectMake(ClipMaxZoon_horizontalMargin, ClipMaxZoon_TopMargin, ScreenW - ClipMaxZoon_horizontalMargin * 2, _stackView.top - ClipMaxZoon_BottomMargin - ClipMaxZoon_TopMargin);
    
    _imageView = [[UIImageView alloc] initWithImage:_image];
    CGSize contentSize = [BCImageEditorHelper convertImageSize:_image.size showInSize:_maxSize.size];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _imageView.frame = (CGRect){CGPointZero,contentSize};
    
    _zoomView = [[BCZoomView alloc] initWithFrame:CGRectMake(0, 0, contentSize.width, contentSize.height) contentView:_imageView];
    [_zoomView allowClipToBounce:NO];
    
    [self.view addSubview:_zoomView];
    [self.view insertSubview:_zoomView atIndex:0];
    
    _gridView =[[BCClipGridView alloc] initWithFrame:self.view.bounds originRect:CGRectZero];
    _gridView.delegate = self;
    [self.view addSubview:_gridView];
    [self.view insertSubview:_zoomView atIndex:0];
}

#pragma mark - delegate -
-(void)willBeginResizeGridRect:(CGRect)rect{
    NSLog(@"%s %@",__func__,NSStringFromCGRect(rect));
    
    CGPoint contentOffset = self.zoomView.scrollView.contentOffset;
    if (self.zoomView.scrollView.contentOffset.x < 0) contentOffset.x = 0;
    if (self.zoomView.scrollView.contentOffset.y < 0) contentOffset.y = 0;
    [self.zoomView.scrollView setContentOffset:contentOffset animated:NO];
}

-(void)didBeginResizeGridRect:(CGRect)rect{
//    NSLog(@"%s %@",__func__,NSStringFromCGRect(rect));
    //要更新zoomview的上下和左右边距 实时计算倍数
    if (_zoomView.scrollView.dragging || _zoomView.scrollView.decelerating) {
        return;
    }
    CGRect gridRect =  rect;
    CGRect imageRect = [_zoomView convertRect:[self.zoomView convertImageToZoomView] toView:self.view];
    
    NSLog(@"imageRect = %@,gridRect = %@",NSStringFromCGRect(imageRect),NSStringFromCGRect(gridRect));
    if (CGRectContainsRect(imageRect, gridRect)) {
        CGPoint contentOffset = self.zoomView.scrollView.contentOffset;
        if (CGRectGetMinY(gridRect) < CGRectGetMinY(imageRect)) contentOffset.y = 0;
        if (CGRectGetMinX(gridRect) < CGRectGetMinX(imageRect)) contentOffset.x = 0;
        _zoomView.scrollView.contentOffset = contentOffset;
        
        _zoomView.frame = gridRect;
        
        CGFloat y_gap = gridRect.size.height > _zoomView.height ? MAX(1, gridRect.size.height / _zoomView.height) : 1;
        CGFloat x_gap = gridRect.size.width > _zoomView.width ? MAX(1, _zoomView.width / gridRect.size.width) : 1;
        NSLog(@"%s %@ x%f y%f num%f ",__func__,NSStringFromCGRect(imageRect),x_gap,y_gap,MAX(x_gap, y_gap));
        _zoomView.scrollView.minimumZoomScale = MAX(x_gap, y_gap);
        [_zoomView.scrollView setZoomScale:_zoomView.scrollView.zoomScale];
    }

}

-(void)didEndResizeGridRect:(CGRect)rect{
    NSLog(@"%s %@",__func__,NSStringFromCGRect(rect));
    //暂停后要停到中间 宽度要到最大的限制宽度 以及
    
}

#pragma mark - user interactor -
- (IBAction)turnImageAround:(id)sender {
    
}

- (IBAction)withdrawImageClip:(id)sender {
    
}

- (IBAction)finishClip:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - getter -

@end
