//
//  BCClipImageViewController.h
//  图片编辑
//
//  Created by aaaa on 2020/3/25.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCClipImageViewController : UIViewController

-(instancetype)initWithImage:(UIImage *)image;



@end

NS_ASSUME_NONNULL_END
