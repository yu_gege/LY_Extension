//
//  BCClipGridLayer.h
//  图片编辑
//
//  Created by aaaa on 2020/3/27.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCClipGridLayer : CAShapeLayer

/// 初始化
/// @param frame 网格视图大小
-(instancetype)initWithFrame:(CGRect)frame;

-(void)setGridRect:(CGRect)gridRect
           animate:(BOOL)animate;

@end

NS_ASSUME_NONNULL_END
