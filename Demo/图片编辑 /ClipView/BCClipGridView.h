//
//  BCClipGridView.h
//  图片编辑
//
//  Created by aaaa on 2020/3/25.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//拖动的方向
typedef enum : NSUInteger {
    BCGridLineDragDirection_None     = 0,
    BCGridLineDragDirection_Left     = 1 << 0,
    BCGridLineDragDirection_Right    = 1 << 1,
    BCGridLineDragDirection_Top      = 1 << 2,
    BCGridLineDragDirection_Bottom   = 1 << 3,
    BCGridLineDragDirection_TLCorner = BCGridLineDragDirection_Top    | BCGridLineDragDirection_Left,
    BCGridLineDragDirection_TRCorner = BCGridLineDragDirection_Top    | BCGridLineDragDirection_Right,
    BCGridLineDragDirection_BLCorner = BCGridLineDragDirection_Bottom | BCGridLineDragDirection_Left,
    BCGridLineDragDirection_BRCorner = BCGridLineDragDirection_Bottom | BCGridLineDragDirection_Right,
} BCGridLineDragDirection;


@protocol BCGridScaleProtocol <NSObject>

-(void)willBeginResizeGridRect:(CGRect)rect;
-(void)didBeginResizeGridRect:(CGRect)rect;
-(void)didEndResizeGridRect:(CGRect)rect;

@end


@interface BCClipGridView : UIView
/// 初始化
/// @param frame 网格视图大小
/// @param rect 裁剪框位置
-(instancetype)initWithFrame:(CGRect)frame
                  originRect:(CGRect)rect;

@property(nonatomic,assign)id<BCGridScaleProtocol>delegate;

-(void)updateRect:(CGRect)rect;

-(void)updateLimitRect:(CGRect)limitRect;

@end

NS_ASSUME_NONNULL_END
