//
//  BCImageEditorViewController.h
//  图片编辑
//
//  Created by aaaa on 2020/3/30.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCImageEditorViewController : UIViewController

-(instancetype)initWithImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
