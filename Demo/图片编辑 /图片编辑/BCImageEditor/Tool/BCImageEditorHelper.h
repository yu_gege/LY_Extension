//
//  BCImageEditorHelper.h
//  图片编辑
//
//  Created by aaaa on 2020/3/30.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface BCImageEditorHelper : NSObject

/// 返回图片在区域里面的合适大小
/// @param imageSize 图片大小
/// @param showInSize 显示范围
+(CGSize)convertImageSize:(CGSize)imageSize
               showInSize:(CGSize)showInSize;


/// 返回图片双击的放大比例
/// @param imageSize 图片大小
/// @param showInSize 显示范围
+(CGFloat)doubleTapScaleWithImageImageSize:(CGSize)imageSize
                                showInSize:(CGSize)showInSize;

@end

NS_ASSUME_NONNULL_END
