//
//  BCImageEditorHelper.m
//  图片编辑
//
//  Created by aaaa on 2020/3/30.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCImageEditorHelper.h"

@implementation BCImageEditorHelper


+(CGSize)convertImageSize:(CGSize)imageSize
               showInSize:(CGSize)showInSize
{
    CGFloat maxHeight = showInSize.height;
    CGFloat maxwidth  = showInSize.width;
    CGFloat imageScale = imageSize.height / imageSize.width;
    
    /// 这里没有对较小图片的处理 要么适配宽 要么适配高
    CGSize newSize = CGSizeMake(maxwidth, maxwidth * imageScale);
    if (newSize.height > maxHeight) {//长图
        newSize = CGSizeMake(maxwidth * maxHeight/newSize.height, maxHeight);
    }
    return newSize;
}


+(CGFloat)doubleTapScaleWithImageImageSize:(CGSize)imageSize showInSize:(CGSize)showInSize{
    CGFloat maxHeight = showInSize.height;
    CGFloat maxwidth  = showInSize.width;
    CGSize newSize = [BCImageEditorHelper convertImageSize:imageSize showInSize:showInSize];
    if (newSize.height == maxHeight) {
        return newSize.width / maxwidth;
    }else{
        return newSize.height / maxHeight;
    }
}

@end

