//
//  BCClipGridLayer.h
//  图片编辑
//
//  Created by aaaa on 2020/3/27.
//  Copyright © 2020 aaaa. All rights reserved.
//
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCImageClipController : UIViewController
@property (nonatomic, strong) UIImage *image;

@property(nonatomic,copy)void(^finishImageClip)(UIImage *image);
@end

NS_ASSUME_NONNULL_END
