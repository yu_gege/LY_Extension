//
//  BCImageEditorDrawView.h
//  图片编辑
//
//  Created by aaaa on 2020/4/1.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCImageEditorDrawView : UIView
//设置线宽度
-(void)setLineWidth:(CGFloat)lineWidth;
//设置线颜色
-(void)setLineColor:(UIColor *)lineColor;
//删除
-(void)deleteBackWard;
//撤销删除
-(void)recallDelete;
//清空画板
-(void)clearDrawBoard;
@property(nonatomic,copy)void(^didFinishDrawing)(int numOfLayer);

@end

NS_ASSUME_NONNULL_END
