//
//  BCDeleteZoomView.h
//  图片编辑
//
//  Created by linyu on 2020/3/31.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCDeleteZoomView : UIView

-(instancetype)initWithFrame:(CGRect)frame;

@property(nonatomic,assign)BOOL inZoom;

-(void)disappear;

-(void)show;
@end

NS_ASSUME_NONNULL_END
