//
//  BCImageEditSqareView.m
//  图片编辑
//
//  Created by aaaa on 2020/4/1.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCImageEditSqareView.h"

@interface BCImageEditSqareView ()

@property(nonatomic,strong)CAShapeLayer *shapeLayer;

@property(nonatomic,strong)CATextLayer *textLayer;

@end

@implementation BCImageEditSqareView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)setText:(NSString *)text size:(CGSize)size color:(UIColor *)color{
    
}

-(void)addSqareLayerWithColor:(UIColor *)color{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.bounds, 5, 5) cornerRadius:2.0];
    path.lineWidth = 2.0;
    path.lineCapStyle = kCGLineCapRound;
    
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.frame = self.bounds;
    _shapeLayer.path = path.CGPath;
    _shapeLayer.fillColor = [UIColor clearColor].CGColor;
    _shapeLayer.strokeColor = color.CGColor;
    [self.layer addSublayer:_shapeLayer];
}


-(CATextLayer *)textLayer{
    if (!_textLayer) {
        _textLayer = [CATextLayer layer];
        _textLayer.font =  CFBridgingRetain([UIFont systemFontOfSize:25.0]);
    }
    return _textLayer;
}

@end
