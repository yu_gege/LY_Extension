//
//  BCImageEditorDrawView.m
//  图片编辑
//
//  Created by aaaa on 2020/4/1.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCImageEditorDrawView.h"

@interface BCImageEditorDrawView()

@property(nonatomic,assign)BOOL isDrawing;
@property(nonatomic,strong)UIColor *stokeColor;
@property(nonatomic,assign)CGFloat lineWidth;

@property(nonatomic,strong)NSMutableArray<UIBezierPath *> *pathArray;
@property(nonatomic,strong)NSMutableArray<CAShapeLayer *> *shapeArray;
@property(nonatomic,strong)NSMutableArray<CAShapeLayer *> *deleteArray;

@end

@implementation BCImageEditorDrawView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = NO;
        self.clipsToBounds = YES;
        self.exclusiveTouch = YES;
        _isDrawing = NO;
        _lineWidth = 5;
    }
    return self;
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
//    NSLog(@"draw hitest");
    return self.userInteractionEnabled ? self : nil;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (touches.count != 1) return;
    NSLog(@"draw touchesBegan");
    if (!_isDrawing) {
        _isDrawing = YES;
                
        UITouch *touch = [touches anyObject];
        CGPoint p = [touch locationInView:self];
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        path.lineWidth = _lineWidth;
        path.lineCapStyle = kCGLineCapRound;
        path.lineJoinStyle = kCGLineJoinRound;
        [path moveToPoint:p];
        [self.pathArray addObject:path];
    
        CAShapeLayer *layer = [self createShapeLayer:path];
        [self.layer addSublayer:layer];
        [self.shapeArray addObject:layer];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"draw moves");
    if (_isDrawing) {
        UITouch *touch = [touches anyObject];
        CGPoint point = [touch locationInView:self];
        UIBezierPath *path = self.pathArray.lastObject;
        if (!CGPointEqualToPoint(path.currentPoint, point)) {
            [path addLineToPoint:point];
            CAShapeLayer *slayer = self.shapeArray.lastObject;
            slayer.path = path.CGPath;
        }
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"draw end");
    _isDrawing = NO;
    !_didFinishDrawing ? : _didFinishDrawing((int)self.shapeArray.count);
}

//取消绘画
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    _isDrawing = NO;
    !_didFinishDrawing ? : _didFinishDrawing((int)self.shapeArray.count);
}

#pragma mark - operation -
-(void)clearDrawBoard{
    [_deleteArray removeAllObjects];
    [_pathArray removeAllObjects];
    [_shapeArray enumerateObjectsUsingBlock:^(CAShapeLayer * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperlayer];
    }];
    [_shapeArray removeAllObjects];
    !_didFinishDrawing ? : _didFinishDrawing(0);
}

//删除
-(void)deleteBackWard{
    if (self.shapeArray.count) {
        CAShapeLayer *layer = self.shapeArray.lastObject;
        [layer removeFromSuperlayer];
        [self.shapeArray removeLastObject];
        [self.pathArray removeLastObject];
        !_didFinishDrawing ? : _didFinishDrawing((int)self.shapeArray.count);
    }
}

//撤销删除
-(void)recallDelete{
    if (self.deleteArray.count) {
        //暂时不做
    }
}

#pragma mark - setter -
-(void)setLineWidth:(CGFloat)lineWidth{
    _lineWidth = lineWidth;
}

-(void)setLineColor:(UIColor *)lineColor{
    _stokeColor = lineColor;
    
}

#pragma mark - getter -
//创建线条图层
/** 用layer的原因
1、渲染快速。CAShapeLayer使用了硬件加速，绘制同一图形会比用Core Graphics快很多。
     Core Graphics实现示例： https://github.com/wsl2ls/Draw.git
2、高效使用内存。一个CAShapeLayer不需要像普通CALayer一样创建一个寄宿图形，所以无论有多大，都不会占用太多的内存。
3、不会被图层边界剪裁掉。
4、不会出现像素化。
 */
- (CAShapeLayer *)createShapeLayer:(UIBezierPath *)path {
    CAShapeLayer *slayer = [CAShapeLayer layer];
    slayer.path = path.CGPath;
    slayer.backgroundColor = [UIColor clearColor].CGColor;
    slayer.fillColor = [UIColor clearColor].CGColor;
    slayer.lineCap = kCALineCapRound;
    slayer.lineJoin = kCALineJoinRound;
    slayer.strokeColor = _stokeColor.CGColor;
    slayer.lineWidth = path.lineWidth;
    return slayer;
}

-(NSMutableArray<UIBezierPath *> *)pathArray{
    if (!_pathArray) {
        _pathArray = @[].mutableCopy;
    }
    return _pathArray;
}

-(NSMutableArray<CAShapeLayer *> *)shapeArray{
    if (!_shapeArray) {
        _shapeArray = @[].mutableCopy;
    }
    return _shapeArray;
}

-(NSMutableArray<CAShapeLayer *> *)deleteArray{
    if (!_deleteArray) {
        _deleteArray = @[].mutableCopy;
    }
    return _deleteArray;
}

@end
