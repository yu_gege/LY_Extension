//
//  BCDeleteZoomView.m
//  图片编辑
//
//  Created by linyu on 2020/3/31.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCDeleteZoomView.h"

static const NSTimeInterval duration = 0.3;

@interface BCDeleteZoomView()

@property(nonatomic,strong)UILabel *label;

@end

@implementation BCDeleteZoomView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.hidden = YES;
        self.alpha = 0;
        self.backgroundColor = RGB_COLOR(247,9,56);
        _label = [UILabel new];
        _label.font = [UIFont systemFontOfSize:15.0];
        _label.textColor = [UIColor whiteColor];
        [self addSubview:_label];
    }
    return self;
}

-(void)show{
    self.hidden = NO;
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)disappear{
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    [_label sizeToFit];
    _label.centerX = self.width * .5;
    _label.top = 15;
}

-(void)setInZoom:(BOOL)inZoom{
    _inZoom = inZoom;
    _label.text = _inZoom ? @"松手即可删除" : @"拖动到此处删除";
    [_label sizeToFit];
}

@end
