//
//  BCZoomView.h
//  图片编辑
//
//  Created by linyu on 2020/3/29.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCZoomView : UIView

/// init
/// @param frame  frame
/// @param contentView imageView
- (instancetype)initWithFrame:(CGRect)frame
                  contentView:(UIImageView *)contentView NS_DESIGNATED_INITIALIZER;

/// 是否允许双击放大
/// @param enable enable
/// @param doubleTapScale 放大的倍数
-(void)enableDoubleTap:(BOOL)enable
             withScale:(CGFloat)doubleTapScale;

///
@property(nonatomic,strong)UIScrollView *scrollView;

/// 设置最大的形变倍数
/// @param maxiumScale 倍数
-(void)setMaxiumScale:(CGFloat)maxiumScale;

///imageview相对于zoomview的中心点
-(CGRect)convertImageToZoomView;

/// 设置是否允许超出视图
-(void)allowClipToBounce:(BOOL)allow;

/// 图片相对于zoomview的中心点
-(CGPoint)imageCenterInZoomView;

/// 设置缩放比例
-(void)setZoomScale:(CGFloat)zoomScale;

/// 当前的缩放比例
-(CGFloat)zoomScale;

@end

NS_ASSUME_NONNULL_END
