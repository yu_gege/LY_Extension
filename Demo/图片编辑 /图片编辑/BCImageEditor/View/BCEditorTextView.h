//
//  BCEditorTextView.h
//  图片编辑
//
//  Created by aaaa on 2020/3/31.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCEditorTextView : UIView

-(instancetype)initWithFrame:(CGRect)frame;
-(instancetype)initWithFrame:(CGRect)frame
                        text:(NSString *)text
                   textColor:(UIColor *)textColor;


@property(nonatomic,copy)void(^didInputText)(CGSize size,NSString *text,UIColor *textColor);

@end

NS_ASSUME_NONNULL_END
