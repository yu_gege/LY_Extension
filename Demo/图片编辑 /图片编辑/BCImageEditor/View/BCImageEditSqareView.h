//
//  BCImageEditSqareView.h
//  图片编辑
//
//  Created by aaaa on 2020/4/1.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCImageEditSqareView : UIView

-(void)addSqareLayerWithColor:(UIColor *)color;
-(void)setText:(NSString *)text size:(CGSize)size color:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
