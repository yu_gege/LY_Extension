//
//  BCZoomView.m
//  图片编辑
//
//  Created by linyu on 2020/3/29.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCZoomView.h"
#import "BCImageEditorDrawView.h"
static const NSUInteger minScale = 1.0;

@interface BCZoomView ()<UIScrollViewDelegate,UIGestureRecognizerDelegate>
///
@property(nonatomic,strong)UIImageView *contentView;
///
@property(nonatomic,strong)UITapGestureRecognizer *doubleTapGesture;
/// 双击放大倍数
@property(nonatomic,assign)CGFloat doubleTapScale;
/// 最大的放大倍数
@property(nonatomic,assign)CGFloat maxiumZoomScale;
@end

@implementation BCZoomView
- (instancetype)initWithFrame:(CGRect)frame
                  contentView:(UIImageView *)contentView
{
    self = [super initWithFrame:frame];
    if (self) {
        _contentView = contentView;
        _maxiumZoomScale = MAXFLOAT;
        _doubleTapScale = 2.0;
        
        [self initUI];
//        [self initGesture];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    NSLog(@"BCZoomView layoutSubviews");
    [UIView animateWithDuration:0.2 animations:^{
        [self->_scrollView setZoomScale:1.0];
        self->_scrollView.frame = self.bounds;
        self->_contentView.center = CGPointMake(self.frame.size.width * .5, self.frame.size.height * .5);
    }];
}

#pragma mark - custom -
-(void)allowClipToBounce:(BOOL)allow{
    self.clipsToBounds = allow;
    self.scrollView.clipsToBounds = allow;
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    int count = (int)self.subviews.count;
    for (int i = count - 1; i >= 0; i--) {
        UIView *chileV =  self.subviews[i];
        CGPoint childP = [self convertPoint:point toView:chileV];
        UIView *fitView = [chileV hitTest:childP withEvent:event];
        if([fitView isKindOfClass:[BCImageEditorDrawView class]] && fitView.userInteractionEnabled){
            self.scrollView.scrollEnabled = NO;
            return fitView;
        }else if (fitView){
            self.scrollView.scrollEnabled = YES;
            return fitView;
        }
    }
    self.scrollView.scrollEnabled = YES;
    return self;
}


#pragma mark - init -
-(void)initUI{
    self.scrollView.frame = self.bounds;
    [self addSubview:self.scrollView];
    CGPoint center = CGPointMake(self.frame.size.width * .5, self.frame.size.height * .5);
    _contentView.center = center;
    [self.scrollView addSubview:_contentView];
}

-(void)initGesture{
    _doubleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDoubleTap:)];
    _doubleTapGesture.numberOfTapsRequired = 2;
    _doubleTapGesture.numberOfTouchesRequired = 1;
    _doubleTapGesture.delegate = self;
    _doubleTapGesture.enabled = NO;
    [self addGestureRecognizer:_doubleTapGesture];
}

#pragma mark - custom method -
-(CGRect)convertImageToZoomView{
    CGRect rect = [_scrollView convertRect:_contentView.frame toViewOrWindow:self];
    return rect;
}
-(void)enableDoubleTap:(BOOL)enable
             withScale:(CGFloat)doubleTapScale{
    _doubleTapGesture.enabled = enable;
}

-(void)setMaxiumScale:(CGFloat)maxiumScale{
    _maxiumZoomScale = maxiumScale;
}

-(void)setZoomScale:(CGFloat)zoomScale{
    [_scrollView setZoomScale:zoomScale];
}

-(CGFloat)zoomScale{
    return _scrollView.zoomScale;
}

-(CGPoint)imageCenterInZoomView{
    return [self convertPoint:self.center toView:self.scrollView];
}

#pragma mark - gesture -
-(void)handleDoubleTap:(UITapGestureRecognizer *)doubleTap{
    if (self.scrollView.zoomScale == minScale) {
        CGPoint touchPoint = [doubleTap locationInView:self.contentView];
        CGFloat newZoomScale = _doubleTapScale;
        CGFloat xSize = self.frame.size.width / newZoomScale;
        CGFloat ySize = self.frame.size.height / newZoomScale;
        CGRect toRect = CGRectMake(touchPoint.x - xSize / 2, touchPoint.y - ySize / 2, xSize, ySize);
        NSLog(@"torect = %@",NSStringFromCGRect(toRect));
        [self.scrollView zoomToRect:toRect animated:YES];
    }else{
        [self.scrollView setZoomScale:minScale animated:YES];
    }
}

#pragma mark - scrollview delegate -
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return _contentView;
}

-(void)scrollViewDidZoom:(UIScrollView *)scrollView{
    NSLog(@"%s",__func__);
    if (scrollView.isZooming || scrollView.isZoomBouncing) {
        // 延中心点缩放
        CGRect rect = CGRectApplyAffineTransform(scrollView.frame, scrollView.transform);
        CGFloat offsetX = (rect.size.width > scrollView.contentSize.width) ? ((rect.size.width - scrollView.contentSize.width) * 0.5) : 0.0;
        CGFloat offsetY = (rect.size.height > scrollView.contentSize.height) ? ((rect.size.height - scrollView.contentSize.height) * 0.5) : 0.0;
        self.contentView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX, scrollView.contentSize.height * 0.5 + offsetY);
    }
}

#pragma mark - getter -
-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]init];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.minimumZoomScale = minScale;
        _scrollView.maximumZoomScale = _maxiumZoomScale;
        _scrollView.delegate = self;
        if (@available(iOS 11.0, *)) {
            _scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            _scrollView.contentInset = UIEdgeInsetsZero;
        }
    }
    return _scrollView;
}


@end
