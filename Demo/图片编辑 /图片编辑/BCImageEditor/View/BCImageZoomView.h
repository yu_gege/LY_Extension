//
//  BCClipGridLayer.h
//  图片编辑
//
//  Created by aaaa on 2020/3/27.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BCImageZoomViewDelegate;
/// 缩放视图 用于图片编辑
@interface BCImageZoomView : UIScrollView
@property (nonatomic, strong) UIImage *image;
//
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, weak) id<BCImageZoomViewDelegate> zoomViewDelegate;
@end

/// 缩放视图代理
@protocol BCImageZoomViewDelegate <NSObject>
@optional
/// 开始移动图像位置
- (void)zoomViewDidBeginMoveImage:(BCImageZoomView *)zoomView;
/// 结束移动图像
- (void)zoomViewDidEndMoveImage:(BCImageZoomView *)zoomView;
@end

NS_ASSUME_NONNULL_END
