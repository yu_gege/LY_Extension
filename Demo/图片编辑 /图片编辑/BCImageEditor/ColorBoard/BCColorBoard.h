//
//  BCColorBoard.h
//  图片编辑
//
//  Created by aaaa on 2020/3/24.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCColorBoard : UIImageView

//从某个点弹出
-(void)showAtPosition:(CGPoint)position;

@property(nonatomic,copy)void(^didselectColor)(UIColor *color);

//消失
-(void)disappear;

-(BOOL)isShowing;
@end

NS_ASSUME_NONNULL_END
