//
//  BCColorItemLayer.h
//  图片编辑
//
//  Created by aaaa on 2020/3/25.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface BCColorItemLayer : CAShapeLayer

-(instancetype)initWithFrame:(CGRect)frame
              backGroudColor:(CGColorRef)color NS_DESIGNATED_INITIALIZER;

@property(nonatomic,assign)BOOL isHightLight;

-(UIColor *)getColor;

@end

NS_ASSUME_NONNULL_END
