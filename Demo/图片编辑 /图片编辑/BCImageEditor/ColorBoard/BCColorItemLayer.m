//
//  BCColorItemLayer.m
//  图片编辑
//
//  Created by aaaa on 2020/3/25.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "BCColorItemLayer.h"
#import <UIKit/UIKit.h>

@implementation BCColorItemLayer
{
    CGColorRef _color;
    CAShapeLayer *_maskLayer;
}

-(instancetype)initWithFrame:(CGRect)frame
              backGroudColor:(CGColorRef)color{
    if (self = [super init]) {
        self.frame = frame;
        _color = color;
        [self addCorner];
    }    
    return self;
}

-(void)addCorner{
    CGFloat radius = 4;
    
    _maskLayer = [[CAShapeLayer alloc] init];
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:radius];
    _maskLayer.path = path.CGPath;
    _maskLayer.fillColor = _color;
    [self addSublayer:_maskLayer];
}

-(void)setIsHightLight:(BOOL)isHightLight{
    _isHightLight = isHightLight;
    _maskLayer.strokeColor = _isHightLight ? [UIColor whiteColor].CGColor : [UIColor clearColor].CGColor;
    _maskLayer.borderWidth = 2.0;
}

-(UIColor *)getColor{
    return [UIColor colorWithCGColor:_color];
}


@end
