//
//  ViewController.m
//  图片编辑
//
//  Created by aaaa on 2020/3/24.
//  Copyright © 2020 aaaa. All rights reserved.
//

#import "ViewController.h"
#import "BCColorBoard.h"
#import "BCClipGridView.h"
#import "BCZoomView.h"
#import "BCImageEditorViewController.h"
#import "BCClipImageViewController.h"
#import "BCImageClipController.h"
@interface ViewController ()<BCGridScaleProtocol>
@property(nonatomic,strong)BCZoomView *zoomView;
@property(nonatomic,strong)BCClipGridView *gridView;

@property(nonatomic,strong)UIView *zoomV;
@end

@implementation ViewController {
    CAShapeLayer *cropLayer;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _zoomV = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 80, 120)];
    _zoomV.backgroundColor = [UIColor redColor];
    UIPanGestureRecognizer *tap = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(ontap:)];
    [self.view addSubview:_zoomV];
    [_zoomV addGestureRecognizer:tap];
    
    _zoomV.transform = CGAffineTransformRotate(_zoomV.transform, 30);
    CGRect rect = CGRectApplyAffineTransform(_zoomV.frame,_zoomV.transform);
    NSLog(@" %@ \n %@",NSStringFromCGRect(_zoomV.frame),NSStringFromCGRect(rect));
}

-(void)ontap:(UIPanGestureRecognizer *)tap{
    CGPoint p = [tap locationInView:_zoomV];
   
    NSLog(@"%@",NSStringFromCGPoint(p));
    CGRect rect = CGRectApplyAffineTransform(_zoomV.frame,_zoomV.transform);
    CGPoint center1 = CGPointMake(rect.size.width * .5, rect.size.height * .5);
    
    CGFloat x_scale = 1 + (center1.x - p.x)/rect.size.width;
    CGFloat y_scale = 1 + (center1.y - p.y)/rect.size.height;
    NSLog(@"%f %f",x_scale,y_scale);
//    _zoomV.transform = CGAffineTransformScale(_zoomV.transform, x_scale, y_scale);
//    _zoomV.transform = CGAffineTransformScale(_zoomV.transform, 1.1, 1.1);
//    _zoomV.transform = CGAffineTransformIdentity;
    [tap setTranslation:CGPointZero inView:_zoomV];

//    _zoomV.transform = CGAffineTransformScale(_zoomV.transform, 1.1, 1.1);
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    BCClipImageViewController *vc = [[BCClipImageViewController alloc] initWithImage:[UIImage imageNamed:@"img.jpg"]];
//    vc.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:vc animated:YES completion:nil];
}

-(void)initUI{
    
    UIImage *image = [UIImage imageNamed:@"img.jpg"];
    UIImageView *imgVIew = [[UIImageView alloc] initWithImage:image];
    CGSize size = image.size;
    CGFloat width;
    CGFloat height;
    if (size.height > size.width) {
        
    }
    
    imgVIew.frame = self.view.bounds;
    _zoomView = [[BCZoomView alloc] initWithFrame:self.view.bounds contentView:imgVIew];
    [self.view addSubview:_zoomView];
    
    CGRect rect = CGRectMake(100, 100, 200, 300);
    _gridView = [[BCClipGridView alloc] initWithFrame:self.view.bounds originRect:rect];
    _gridView.delegate = self;
    [self.view addSubview:_gridView];
}

-(void)colorBoard{
    BCColorBoard *board = [BCColorBoard new];
    [self.view addSubview:board];
    board.center = CGPointMake(200, 350);
}

#pragma mark - delegate -
- (void)willBeginResizeGridRect:(CGRect)rect {
    NSLog(@"%s  rect = %@",__func__,NSStringFromCGRect(rect));
}

- (void)didBeginResizeGridRect:(CGRect)rect {
    NSLog(@"%s  rect = %@",__func__,NSStringFromCGRect(rect));
}

- (void)didEndResizeGridRect:(CGRect)rect {
     NSLog(@"%s  rect = %@",__func__,NSStringFromCGRect(rect));
    
}

#pragma mark - 两种镂空遮罩 -
//原理 一个方形的路径 跟view一样大 在其中拼接一个圆形的路径 然后设置圆形遮罩的填充颜色 并修改填充模式 填充到圆形外面
-(void)clipView1{
    CAShapeLayer* cropLayer = [[CAShapeLayer alloc] init];

    // 创建一个绘制路径
    CGMutablePathRef path =CGPathCreateMutable();
    // 空心矩形的rect
    CGRect cropRect = CGRectMake(20, 30, 60, 40);
    // 绘制rect
    CGPathAddRect(path, nil, self.view.bounds);
    CGPathAddRect(path, nil, cropRect);
    // 设置填充规则(重点)
    [cropLayer setFillRule:kCAFillRuleEvenOdd];
    // 关联绘制的path
    [cropLayer setPath:path];
    // 设置填充的颜色
    [cropLayer setFillColor:[[UIColor colorWithWhite:0 alpha:0.5] CGColor]];

    [self.view.layer addSublayer:cropLayer];
}

-(void)clipView{
    CAShapeLayer *pShapeLayer = [CAShapeLayer layer];
    pShapeLayer.fillColor = [UIColor redColor].CGColor;
    [self.view.layer addSublayer:pShapeLayer];
    //方形遮罩
    UIBezierPath *pPath = [UIBezierPath bezierPathWithRect:CGRectMake(40, 40, 50, 50)];
    pShapeLayer.path = pPath.CGPath;

    //外面的路径
    UIBezierPath *pOtherPath = [UIBezierPath bezierPathWithRect:self.view.bounds];
    pShapeLayer.path = pOtherPath.CGPath;
    [pOtherPath appendPath:pPath];
    pShapeLayer.path = pOtherPath.CGPath;
    //重点
    pShapeLayer.fillRule = kCAFillRuleEvenOdd;
}

@end
