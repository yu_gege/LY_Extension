//
//  ViewController.m
//  LYAppMoudle
//
//  Created by 林域 on 2018/12/17.
//  Copyright © 2018 林域. All rights reserved.
//

#import "ViewController.h"
// -- Sand Box -- //
#define LYSandBox_Document_BasePath NSHomeDirectory()
#define LYSandBox_Document_Path [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
#define LYSandBox_Libarary_Path [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject]
#define LYSandBox_Cache_Path [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]
#define LYSandBox_Temp_Path NSTemporaryDirectory()

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",LYSandBox_Document_BasePath);
    NSLog(@"%@",LYSandBox_Document_Path);
    NSLog(@"%@",LYSandBox_Libarary_Path);
    NSLog(@"%@",LYSandBox_Cache_Path);
    NSLog(@"%@",LYSandBox_Temp_Path);
}


@end
