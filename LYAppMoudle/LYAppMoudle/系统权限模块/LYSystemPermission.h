//
//  LYSystemPermission.h
//  LYAppMoudle
//
//  Created by 林域 on 2018/12/18.
//  Copyright © 2018 林域. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    
    LYPermissionType_Camera,
    LYPermissionType_Photo,
    LYPermissionType_NetWork,
    LYPermissionType_Contact,
    LYPermissionType_MicroPhone,
    LYPermissionType_Notification,
    LYPermissionType_Calendar,
    LYPermissionType_BlueTooth,
    
    //location
    LYPermissionType_Location,
    LYPermissionType_LocationWhenInUse,
    LYPermissionType_LocationAlways,
    
} LYPermissionType;

NS_ASSUME_NONNULL_BEGIN

typedef void(^handlePermission)(void);
typedef void(^unhandlePermission)(void);

@interface LYSystemPermission : NSObject

+ (instancetype)sharedInstance;


- (void)LYPermission_RequestAuthorizationWithType:(LYPermissionType)type
                                 PermissionHandle:(handlePermission)handle
                                         unHandle:(unhandlePermission)unHandle;
@end

NS_ASSUME_NONNULL_END
