//
//  LYLanguageManager.h
//  LYAppMoudle
//
//  Created by 林域 on 2018/12/25.
//  Copyright © 2018 林域. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LYLanguageManager : NSObject

+ (BOOL)isChinese;

+ (BOOL)ly_setCurrentLanguage:(NSString *)language;

+ (NSString *)ly_getCurrentLanguage;

@end

NS_ASSUME_NONNULL_END
