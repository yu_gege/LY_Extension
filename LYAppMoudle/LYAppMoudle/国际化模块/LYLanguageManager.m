//
//  LYLanguageManager.m
//  LYAppMoudle
//
//  Created by 林域 on 2018/12/25.
//  Copyright © 2018 林域. All rights reserved.
//

#import "LYLanguageManager.h"
#import "LYCommonBlock.h"
static NSString *const Language_Key = @"LYAPP_Language_Key";

@implementation LYLanguageManager

+ (BOOL)isChinese{    
    NSString *appLanguage = LYUserDefaultObjForKey(Language_Key);    
    if (appLanguage.length > 0) {
        if ([appLanguage hasPrefix:@"zh-Hans"] || [appLanguage hasPrefix:@"zh-Hant"] || [appLanguage hasPrefix:@"zh-Hant-TW"]) {
            return YES;
        }
        return NO;
    }
    return NO;
}

+ (BOOL)ly_setCurrentLanguage:(NSString *)language {
    if (language == nil || language.length == 0) {
        return NO;
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:language forKey:Language_Key];
    [userDefaults synchronize];
    return YES;
}

+ (NSString *)ly_getCurrentLanguage{
    NSString *language = LYUserDefaultObjForKey(Language_Key);
    return language?:LY_SYS_CurrentLanguage;
}

@end
