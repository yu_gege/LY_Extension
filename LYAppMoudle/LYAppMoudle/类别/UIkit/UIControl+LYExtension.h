//
//  UIControl+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (LYExtension)

//不同事件增加不同声音
- (void)ly_setSoundNamed:(NSString *)name forControlEvent:(UIControlEvents)controlEvent;

#pragma mark - Interactive Block -
- (void)ly_touchDown:(void (^)(void))eventBlock;
- (void)ly_touchDownRepeat:(void (^)(void))eventBlock;
- (void)ly_touchDragInside:(void (^)(void))eventBlock;
- (void)ly_touchDragOutside:(void (^)(void))eventBlock;
- (void)ly_touchDragEnter:(void (^)(void))eventBlock;
- (void)ly_touchDragExit:(void (^)(void))eventBlock;
- (void)ly_touchUpInside:(void (^)(void))eventBlock;
- (void)ly_touchUpOutside:(void (^)(void))eventBlock;
- (void)ly_touchCancel:(void (^)(void))eventBlock;
- (void)ly_valueChanged:(void (^)(void))eventBlock;
- (void)ly_editingDidBegin:(void (^)(void))eventBlock;
- (void)ly_editingChanged:(void (^)(void))eventBlock;
- (void)ly_editingDidEnd:(void (^)(void))eventBlock;
- (void)ly_editingDidEndOnExit:(void (^)(void))eventBlock;

@end
