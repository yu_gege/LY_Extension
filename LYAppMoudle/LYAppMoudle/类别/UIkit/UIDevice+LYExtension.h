//
//  UIDevice+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (LYExtension)

+ (NSString *)ly_platform;
+ (NSString *)ly_platformString;


+ (NSString *)ly_macAddress;

//Return the current device CPU frequency
+ (NSUInteger)ly_cpuFrequency;
// Return the current device BUS frequency
+ (NSUInteger)ly_busFrequency;
//current device RAM size
+ (NSUInteger)ly_ramSize;
//Return the current device CPU number
+ (NSUInteger)ly_cpuNumber;
//Return the current device total memory

/// 获取iOS系统的版本号
+ (NSString *)ly_systemVersion;
/// 判断当前系统是否有摄像头
+ (BOOL)ly_hasCamera;
/// 获取手机内存总量, 返回的是字节数
+ (NSUInteger)ly_totalMemoryBytes;
/// 获取手机可用内存, 返回的是字节数
+ (NSUInteger)ly_freeMemoryBytes;

/// 获取手机硬盘空闲空间, 返回的是字节数
+ (long long)ly_freeDiskSpaceBytes;
/// 获取手机硬盘总空间, 返回的是字节数
+ (long long)ly_totalDiskSpaceBytes;

@end
