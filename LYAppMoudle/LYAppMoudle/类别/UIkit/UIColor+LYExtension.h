//
//  UIColor+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/4.
//  Copyright © 2018 林域. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RGBA_COLOR(R, G, B, A) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:A]
#define RGB_COLOR(R, G, B) [UIColor colorWithRed:((R) / 255.0f) green:((G) / 255.0f) blue:((B) / 255.0f) alpha:1.0f]
#define HEXCOLOR(String) [UIColor colorWithHexString:String]
#define HEXCOLORA(String,Alpha) [UIColor colorWithHexString:String alpha:Alpha]

@interface UIColor (LYExtension)
//从十六进制字符串获取颜色，
//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)colorWithHexString:(NSString *)color;
+ (UIColor *)colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;


@end
