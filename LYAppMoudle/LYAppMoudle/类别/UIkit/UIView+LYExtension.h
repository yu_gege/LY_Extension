//
//  UIView+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, LYShakeDirection) {
    LYShakeDirectionHorizontal = 0,
    LYShakeDirectionVertical
};

typedef NS_OPTIONS(NSUInteger, LYExcludePoint) {
    LYExcludeStartPoint = 1 << 0,
    LYExcludeEndPoint = 1 << 1,
    LYExcludeAllPoint = ~0UL
};

@interface UIView (LYExtension)

#pragma mark - controller 控制器 -

- (UIViewController *)CurrentViewController;


#pragma mark - Frame布局 -

// shortcuts for frame properties
@property (nonatomic, assign) CGPoint ly_origin;
@property (nonatomic, assign) CGSize ly_size;

// shortcuts for positions
@property (nonatomic) CGFloat ly_centerX;
@property (nonatomic) CGFloat ly_centerY;


@property (nonatomic) CGFloat ly_top;
@property (nonatomic) CGFloat ly_bottom;
@property (nonatomic) CGFloat ly_right;
@property (nonatomic) CGFloat ly_left;

@property (nonatomic) CGFloat ly_width;
@property (nonatomic) CGFloat ly_height;


#pragma mark - Border边框 -

- (void)ly_addTopBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth;
- (void)ly_addLeftBorderWithColor: (UIColor *) color width:(CGFloat) borderWidth;
- (void)ly_addBottomBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth;
- (void)ly_addRightBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth;

- (void)ly_removeTopBorder;
- (void)ly_removeLeftBorder;
- (void)ly_removeBottomBorder;
- (void)ly_removeRightBorder;


- (void)ly_addTopBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(LYExcludePoint)edge;
- (void)ly_addLeftBorderWithColor: (UIColor *) color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(LYExcludePoint)edge;
- (void)ly_addBottomBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(LYExcludePoint)edge;
- (void)ly_addRightBorderWithColor:(UIColor *)color width:(CGFloat) borderWidth excludePoint:(CGFloat)point edgeType:(LYExcludePoint)edge;


#pragma mark - shake animation 动画 -


/**-----------------------------------------------------------------------------
 * @name UIView+Shake
 * -----------------------------------------------------------------------------
 */

/** Shake the UIView
 *
 * Shake the view a default number of times
 */
- (void)ly_shake;

/** Shake the UIView
 *
 * Shake the view a given number of times
 *
 * @param times The number of shakes
 * @param delta The width of the shake
 */
- (void)ly_shake:(int)times withDelta:(CGFloat)delta;

/** Shake the UIView
 *
 * Shake the view a given number of times
 *
 * @param times The number of shakes
 * @param delta The width of the shake
 * @param handler A block object to be executed when the shake sequence ends
 */
- (void)ly_shake:(int)times withDelta:(CGFloat)delta completion:(void((^)(void)))handler;

/** Shake the UIView at a custom speed
 *
 * Shake the view a given number of times with a given speed
 *
 * @param times The number of shakes
 * @param delta The width of the shake
 * @param interval The duration of one shake
 */
- (void)ly_shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval;

/** Shake the UIView at a custom speed
 *
 * Shake the view a given number of times with a given speed
 *
 * @param times The number of shakes
 * @param delta The width of the shake
 * @param interval The duration of one shake
 * @param handler A block object to be executed when the shake sequence ends
 */
- (void)ly_shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval completion:(void((^)(void)))handler;

/** Shake the UIView at a custom speed
 *
 * Shake the view a given number of times with a given speed
 *
 * @param times The number of shakes
 * @param delta The width of the shake
 * @param interval The duration of one shake
 * @param shakeDirection    direction of the shake
 */
- (void)ly_shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval shakeDirection:(LYShakeDirection)shakeDirection;

/** Shake the UIView at a custom speed
 *
 * Shake the view a given number of times with a given speed, with a completion handler
 *
 * @param times The number of shakes
 * @param delta The width of the shake
 * @param interval The duration of one shake
 * @param shakeDirection    direction of the shake
 * @param completion to be called when the view is done shaking
 */
- (void)ly_shake:(int)times withDelta:(CGFloat)delta speed:(NSTimeInterval)interval shakeDirection:(LYShakeDirection)shakeDirection completion:(void(^)(void))completion;

#pragma mark - Visuals 阴影圆角相关 -
/*
 *  Sets a corners with radius, given stroke size & color
 */
-(void)ly_cornerRadius: (CGFloat)radius
            strokeSize: (CGFloat)size
                 color: (UIColor *)color;
/*
 *  Sets a corners
 */
-(void)ly_setRoundedCorners:(UIRectCorner)corners
                     radius:(CGFloat)radius;

/*
 *  Draws shadow with properties
 */
-(void)ly_shadowWithColor: (UIColor *)color
                   offset: (CGSize)offset
                  opacity: (CGFloat)opacity
                   radius: (CGFloat)radius;

/*
 *  Removes from superview with fade
 */
-(void)ly_removeFromSuperviewWithFadeDuration: (NSTimeInterval)duration;

/*
 *  Adds a subview with given transition & duration
 */
-(void)ly_addSubview: (UIView *)view withTransition: (UIViewAnimationTransition)transition duration: (NSTimeInterval)duration;

/*
 *  Removes view from superview with given transition & duration
 */
-(void)ly_removeFromSuperviewWithTransition: (UIViewAnimationTransition)transition duration: (NSTimeInterval)duration;

/*
 *  Rotates view by given angle. TimingFunction can be nil and defaults to kCAMediaTimingFunctionEaseInEaseOut.
 */
-(void)ly_rotateByAngle: (CGFloat)angle
               duration: (NSTimeInterval)duration
            autoreverse: (BOOL)autoreverse
            repeatCount: (CGFloat)repeatCount
         timingFunction: (CAMediaTimingFunction *)timingFunction;

/*
 *  Moves view to point. TimingFunction can be nil and defaults to kCAMediaTimingFunctionEaseInEaseOut.
 */
-(void)ly_moveToPoint: (CGPoint)newPoint
             duration: (NSTimeInterval)duration
          autoreverse: (BOOL)autoreverse
          repeatCount: (CGFloat)repeatCount
       timingFunction: (CAMediaTimingFunction *)timingFunction;


@end
