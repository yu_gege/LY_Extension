//
//  UIButton+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LYImagePosition) {
    LYImagePositionLeft = 0,              //图片在左，文字在右，默认
    LYImagePositionRight = 1,             //图片在右，文字在左
    LYImagePositionTop = 2,               //图片在上，文字在下
    LYImagePositionBottom = 3,            //图片在下，文字在上
};

@interface UIButton (LYExtension)

/**
 *  利用UIButton的titleEdgeInsets和imageEdgeInsets来实现文字和图片的自由排列
 *  注意：这个方法需要在设置图片和文字之后才可以调用，且button的大小要大于 图片大小+文字大小+spacing
 *
 *  @param spacing 图片和文字的间隔
 */
- (void)ly_setImagePosition:(LYImagePosition)postion spacing:(CGFloat)spacing;

/**
 *  @brief  设置按钮额外热区
 */
@property (nonatomic, assign) UIEdgeInsets ly_touchAreaInsets;

#pragma mark - 倒计时按钮 -
-(void)ly_startTime:(NSInteger )timeout title:(NSString *)tittle waitTittle:(NSString *)waitTittle;


@end
