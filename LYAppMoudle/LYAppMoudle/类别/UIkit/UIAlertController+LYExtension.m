//
//  UIAlertController+LYExtension.m
//  LYAppMoudle
//
//  Created by 林域 on 2019/1/21.
//  Copyright © 2019 林域. All rights reserved.
//

#import "UIAlertController+LYExtension.h"
@implementation UIAlertController (LYExtension)

+(void)showWithTitle:(NSString *)title VC:(UIViewController *)vc action:(void(^)(void))userAction{
    
    UIAlertController *alertControl = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertControl addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (userAction) { userAction(); }
    }]];
    [alertControl addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [vc presentViewController:alertControl animated:YES completion:nil];
    
}


@end
