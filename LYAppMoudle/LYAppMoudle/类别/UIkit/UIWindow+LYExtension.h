//
//  UIWindow+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (LYExtension)

/// get visuable 
+ (UIViewController *)CurrentViewController;

@end
