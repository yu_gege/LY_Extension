//
//  LYCategory.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.

#ifndef LYCategory_h
#define LYCategory_h

#pragma mark - UIKit -
#import "UIColor+LYExtension.h"
#import "UIControl+LYExtension.h"
#import "UIWindow+LYExtension.h"
#import "UIDevice+LYExtension.h"
#import "UIButton+LYExtension.h"
#import "UIImage+LYExtension.h"
#import "UINavigationController+LYExtension.h"
#import "UIView+LYExtension.h"

#pragma mark - Foundation -
#import "NSFileManager+LYExtension.h"
#import "NSObject+LYExtension.h"
#import "NSString+LYExtension.h"
#import "NSDictionary+LYExtension.h"

#endif /* LYCategory_h */
