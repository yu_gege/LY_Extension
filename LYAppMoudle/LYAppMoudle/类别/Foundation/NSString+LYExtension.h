//
//  NSString+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/5.
//  Copyright © 2018 林域. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/* Format **/
extern NSString* StringWithInt(int digit);
extern NSString* StringWithFloat(float digit);
extern NSString* StringWithlong(long digit);
extern NSString* StringWithInteger(NSInteger digit);
extern NSString* StringWithCGFloat(CGFloat digit);
extern NSString* StringWithNSUInteger(NSUInteger digit);

@interface NSString (LYExtension)

#pragma mark - 拼音 -
// 音标
- (NSString *)ly_pinyinWithPhoneticSymbol;
// 拼音
- (NSString *)ly_pinyin;
// 拼音数组
- (NSArray *)ly_pinyinArray;
// 无空格
- (NSString *)ly_pinyinWithoutBlank;
//
- (NSArray *)ly_pinyinInitialsArray;
//
- (NSString *)ly_pinyinInitialsString;

#pragma mark - 字符串范围 -
// 计算文字的高度
- (CGFloat)ly_heightWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width;

// 计算文字的宽度
- (CGFloat)ly_widthWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height;

// 计算文字的大小
- (CGSize)ly_sizeWithFont:(UIFont *)font constrainedToWidth:(CGFloat)width;

// 计算文字的大小
- (CGSize)ly_sizeWithFont:(UIFont *)font constrainedToHeight:(CGFloat)height;

// 反转字符串
+ (NSString *)ly_reverseString:(NSString *)strSrc;

#pragma mark - Emoji -

///移除所有emoji，以“”替换
- (NSString *)ly_removingEmoji;
///移除所有emoji，以“”替换
- (NSString *)ly_stringByRemovingEmoji;
///移除所有emoji，以string替换
- (NSString *)ly_stringByReplaceingEmojiWithString:(NSString*)string;

///字符串是否包含emoji
- (BOOL)ly_containsEmoji;
///字符串中包含的所有emoji unicode格式
- (NSArray<NSString *>*)ly_allEmoji;
///字符串中包含的所有emoji
- (NSString *)ly_allEmojiString;
///字符串中包含的所有emoji rang
- (NSArray<NSString *>*)ly_allEmojiRanges;

///所有emoji表情
+ (NSString*)ly_allSystemEmoji;

@end
