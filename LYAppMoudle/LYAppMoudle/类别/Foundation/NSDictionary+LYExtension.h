//
//  NSDictionary+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/6.
//  Copyright © 2018 林域. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (LYExtension)

-(void)ly_setValue:(nullable id)value forKey:(nonnull NSString *)key;

-(NSString *)ly_JSONString;

// 将url参数转换成NSDictionary
+ (NSDictionary *)ly_dictionaryWithURLQuery:(NSString *)query;

// 将NSDictionary转换成url 参数字符串
- (NSString *)ly_URLQueryString;

@end


@interface NSMutableDictionary (LYExtension)

-(void)ly_setObject:(nullable id)object forKey:(nonnull NSString *)key;

-(NSString *)ly_JSONString;

@end
