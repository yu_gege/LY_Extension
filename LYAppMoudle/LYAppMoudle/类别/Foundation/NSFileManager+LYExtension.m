//
//  NSFileManager+LYExtension.m
//  LYAppMoudle
//
//  Created by 林域 on 2019/1/21.
//  Copyright © 2019 林域. All rights reserved.
//

#import "NSFileManager+LYExtension.h"

#define FileManager [NSFileManager defaultManager]

@implementation NSFileManager (LYExtension)



+ (BOOL)ly_FileExistsAtPath:(NSString *)path{
    NSParameterAssert(path);
    return [FileManager fileExistsAtPath:path];
}

@end
