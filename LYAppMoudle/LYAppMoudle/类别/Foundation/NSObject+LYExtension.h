//
//  NSObject+LYExtension.h
//  即时通讯
//
//  Created by 林域 on 2018/12/4.
//  Copyright © 2018 林域. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (LYExtension)

//- (int)retainCount;

//类名
- (NSString *)ly_className;
+ (NSString *)ly_className;

//父类名称
- (NSString *)ly_superClassName;
+ (NSString *)ly_superClassName;

//实例属性字典
-(NSDictionary *)ly_propertyDictionary;

//属性名称列表
- (NSArray*)ly_propertyKeys;
+ (NSArray *)ly_propertyKeys;

//属性详细信息列表
- (NSArray *)ly_propertiesInfo;
+ (NSArray *)ly_propertiesInfo;

//格式化后的属性列表
+ (NSArray *)ly_propertiesWithCodeFormat;

//方法列表
-(NSArray*)ly_methodList;
+(NSArray*)ly_methodList;

-(NSArray*)ly_methodListInfo;

//创建并返回一个指向所有已注册类的指针列表
+ (NSArray *)ly_registedClassList;
//实例变量
+ (NSArray *)ly_instanceVariable;

//协议列表
-(NSDictionary *)ly_protocolList;
+ (NSDictionary *)ly_protocolList;


- (BOOL)ly_hasPropertyForKey:(NSString*)key;
- (BOOL)ly_hasIvarForKey:(NSString*)key;

@end
