//
//  NSNull+Safe.h
//  LYAppMoudle
//
//  Created by linyu on 2020/10/26.
//  Copyright © 2020 林域. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSNull (Safe)

@end

NS_ASSUME_NONNULL_END
