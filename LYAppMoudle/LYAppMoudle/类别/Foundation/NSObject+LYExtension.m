//
//  NSObject+LYExtension.m
//  即时通讯
//
//  Created by 林域 on 2018/12/4.
//  Copyright © 2018 林域. All rights reserved.
//

#import "NSObject+LYExtension.h"
#import <objc/runtime.h>

@implementation NSObject (LYExtension)

//- (int)retainCount{
//    return CFGetRetainCount((__bridge CFTypeRef)self));
//}

//类名
- (NSString *)ly_className{
    return NSStringFromClass([self class]);
}

+ (NSString *)ly_className{
    return NSStringFromClass([self class]);
}

//父类名称
- (NSString *)ly_superClassName{
     return NSStringFromClass([self superclass]);
}

+ (NSString *)ly_superClassName{
     return NSStringFromClass([self superclass]);
}

//属性列表
-(NSDictionary *)ly_propertyDictionary
{
    //创建可变字典
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    unsigned int outCount;
    objc_property_t *props = class_copyPropertyList([self class], &outCount);
    for(int i=0;i<outCount;i++){
        objc_property_t prop = props[i];
        NSString *propName = [[NSString alloc]initWithCString:property_getName(prop) encoding:NSUTF8StringEncoding];
        id propValue = [self valueForKey:propName];
        [dict setObject:propValue?:[NSNull null] forKey:propName];
    }
    free(props);
    return dict;
}


- (NSArray*)ly_propertyKeys
{
    return [[self class] ly_propertyKeys];
}

+ (NSArray *)ly_propertyKeys {
    unsigned int propertyCount = 0;
    objc_property_t * properties = class_copyPropertyList(self, &propertyCount);
    NSMutableArray * propertyNames = [NSMutableArray array];
    for (unsigned int i = 0; i < propertyCount; ++i) {
        objc_property_t property = properties[i];
        const char * name = property_getName(property);
        [propertyNames addObject:[NSString stringWithUTF8String:name]];
    }
    free(properties);
    return propertyNames;
}



@end
