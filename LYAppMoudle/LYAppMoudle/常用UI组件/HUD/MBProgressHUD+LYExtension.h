//
//  MBProgressHUD+LYExtension.h
//  LYAppMoudle
//
//  Created by 林域 on 2019/1/14.
//  Copyright © 2019 林域. All rights reserved.
//

#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBProgressHUD (LYExtension)

+(MBProgressHUD *)showHUDWithView:(UIView *__nonnull)view;
+(MBProgressHUD *)showHUDWithTitle:(NSString *)title View:(UIView *__nonnull)view;
+(MBProgressHUD *)showHUDWithView:(UIView *__nonnull)view duration:(float)duration;
+(MBProgressHUD *)showHUDOnWindowWithDuration:(float)duration;

+(MBProgressHUD *)showWithTitle:(NSString *)title view:(UIView *)view duration:(float)duration;
+(MBProgressHUD *)showWithTitle:(NSString *)title view:(UIView *)view CustomView:(UIView *)CustomView duration:(float)duration;

@end

NS_ASSUME_NONNULL_END
