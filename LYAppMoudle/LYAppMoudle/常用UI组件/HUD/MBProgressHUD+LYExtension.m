//
//  MBProgressHUD+LYExtension.m
//  LYAppMoudle
//
//  Created by 林域 on 2019/1/14.
//  Copyright © 2019 林域. All rights reserved.
//

#import "MBProgressHUD+LYExtension.h"

#define HUD_Default_Duration 1.0


@implementation MBProgressHUD (LYExtension)

#pragma mark - with activity -
+(MBProgressHUD *)showHUDWithView:(UIView *__nonnull)view{
    MBProgressHUD *hud = [MBProgressHUD showHUDWithView:view duration:MAXFLOAT];
    return hud;
}

+(MBProgressHUD *)showHUDWithTitle:(NSString *)title View:(UIView *__nonnull)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDWithView:view duration:HUD_Default_Duration];
    hud.label.text = title;
    return hud;
}

+(MBProgressHUD *)showHUDWithView:(UIView *__nonnull)view duration:(float)duration
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    [hud hideAnimated:YES afterDelay:duration];
    return hud;
}

+(MBProgressHUD *)showHUDOnWindowWithDuration:(float)duration
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [hud hideAnimated:YES afterDelay:duration];
    return hud;
}


#pragma mark - no activity -
+(MBProgressHUD *)showWithTitle:(NSString *)title view:(UIView *)view duration:(float)duration{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.text = title;
    [hud hideAnimated:YES afterDelay:duration];
    return hud;
}

+(MBProgressHUD *)showWithTitle:(NSString *)title view:(UIView *)view CustomView:(UIView *)CustomView duration:(float)duration{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = view;
    hud.square = YES;
    hud.label.text = title;
    [hud hideAnimated:YES afterDelay:duration];
    return hud;
}


@end
